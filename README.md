# a-see-parser
A simple, easy, elegant (or silly, elementary, experimental) general purpose parser.
Allows a PEG grammar to be specified in C.


## Example

PEG grammar for simple calculater

    expr <- term !.

    term <- factor ( ( '+' / '-' ) factor )*

    factor <- ( ('+' / '-' ) factor ) / ( power ( ( '*' / '/' ) power)* )

    power <- value ( '^' power )*

    value <- number / '(' term ')' / function / variable

    function <- ident '(' term ')'

    variable <- ident

Equivalent using a-see-parser (see examples/calculator directory for complete code)

    #include <a_see_parser.h>
    #define SPACE   ZERO_OR_MORE(NEXT_CHR_IS(' '),,)
    #define PLUS    NEXT_CHR_IS('+') && SPACE
    #define MINUS   NEXT_CHR_IS('-') && SPACE
    #define TIMES   NEXT_CHR_IS('*') && SPACE
    #define DIVIDE  NEXT_CHR_IS('/') && SPACE
    #define POWER   NEXT_CHR_IS('^') && SPACE

    int expr()
    {
      return SPACE && term() && !ANY;
    }

    int term()
    {
      return factor() && ZERO_OR_MORE((PLUS || MINUS) && factor(),,);
    }

    int factor()
    {
      return SEQUENCE((PLUS || MINUS) && factor(),,)
      ||
      (power() && ZERO_OR_MORE((TIMES || DIVIDE) && power(),,));
    }

    int power()
    {
      return value() && ZERO_OR_MORE(POWER && power(),,);
    }

    int value()
    {
      return number()
        ||
        SEQUENCE(LPAREN && term() && RPAREN,,)
        ||
        function()
        ||
        variable();
    }

    int number()
    {
      return SEQUENCE(REAL || INTEGER,,);
    }

    int function()
    {
      return SEQUENCE(IDENTIFIER && LPAREN && term() && RPAREN,,);
    }

    int variable()
    {
      return SEQUENCE(IDENTIFIER,,);
    }
