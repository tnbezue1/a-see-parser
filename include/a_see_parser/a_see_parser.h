/*
		Copyright (C) 2021  by Terry N Bezue

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __A_SEE_PARSER_INCLUDED__
#define __A_SEE_PARSER_INCLUDED__

#ifdef __cplusplus
extern "C" {
#endif
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#ifdef A_SEE_PARSER_LOCAL
# ifndef A_SEE_PARSER_MEMBERS
#   define A_SEE_PARSER_MEMBERS
# endif
# ifndef A_SEE_PARSER_INPUT
# define A_SEE_PARSER_INPUT(ACP_PTR,buf,result,max_size) \
    {\
    int c= getchar();                          \
    result= (EOF == c) ? 0 : (*(buf)= c, 1);\
    }
# endif
#else
# define A_SEE_PARSER_MEMBERS
# include <stdio.h>
# ifndef A_SEE_PARSER_INPUT
#   define A_SEE_PARSER_INPUT(buf,result,max_size) \
      {\
      int c= getchar();                          \
      result= (EOF == c) ? 0 : (*(buf)= c, 1);\
      }
# endif
#endif

/*
  base buffer class
*/
typedef struct {
  char *buffer_;
  unsigned int capacity_;
} a_see_parser_buffer_t;

void a_see_parser_buffer_increase_capacity(a_see_parser_buffer_t* this,unsigned int new_capacity)
{
  if(new_capacity > this->capacity_)
    this->buffer_=(char*)realloc(this->buffer_,(this->capacity_=new_capacity)+4); // a little extra for terminating byte
}

void a_see_parser_buffer_init(a_see_parser_buffer_t* this,unsigned int initial_capacity)
{
  this->buffer_=NULL;
  this->capacity_=0;
  a_see_parser_buffer_increase_capacity(this,initial_capacity);
}

void a_see_parser_buffer_destroy(a_see_parser_buffer_t* this)
{
  free(this->buffer_);
}

#ifndef A_SEE_PARSER_CAPTURE_BUFFER_SIZE
#define A_SEE_PARSER_CAPTURE_BUFFER_SIZE 32
#endif

/*
  Input buffer
*/
typedef struct {
  char* buffer_;
  unsigned int capacity_;
  // indicates if more data (that is, last ivocation of INPUT macro returned no data)
  unsigned size_;
  int no_more_data_;
} a_see_parser_input_buffer_t;

void a_see_parser_input_buffer_init(a_see_parser_input_buffer_t* this,unsigned int initial_capacity)
{
  a_see_parser_buffer_init((a_see_parser_buffer_t*)this,initial_capacity);
}

void a_see_parser_input_buffer_increase_capacity(a_see_parser_input_buffer_t* this,unsigned int new_capacity)
{
  a_see_parser_buffer_increase_capacity((a_see_parser_buffer_t*)this,new_capacity);
}

void a_see_parser_input_buffer_destroy(a_see_parser_input_buffer_t* this)
{
  a_see_parser_buffer_destroy((a_see_parser_buffer_t*)this);
}

#ifndef A_SEE_PARSER_INPUT_BUFFER_SIZE
#define A_SEE_PARSER_INPUT_BUFFER_SIZE 1024
#endif

/*
  This structure holds current state of parser
*/
typedef struct {
  unsigned int pos_;
  unsigned int capture_begin_;
  unsigned int capture_end_;
  unsigned int line_;
  unsigned int col_;
} a_see_parser_data_t;

/*
  The a_see_parser struct -- combines the above parts
*/
typedef struct {
  a_see_parser_buffer_t yytext_;
  a_see_parser_input_buffer_t input_;
  a_see_parser_data_t data_;
  unsigned int max_pos_; // the farthest point in buffer consummed -- can be useful for debugging
  A_SEE_PARSER_MEMBERS
} a_see_parser_t;

// Save/Restore state of parser
#define A_SEE_PARSER_SAVE_STATE(ACP_PTR) a_see_parser_data_t __local_data__ = (ACP_PTR)->data_
#define A_SEE_PARSER_RESTORE_STATE(ACP_PTR) (ACP_PTR)->data_ = __local_data__

#define A_SEE_PARSER_FUNCIION_ATTRIBUTE static inline
A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_capture_begin(a_see_parser_t* acp)
{
  acp->data_.capture_begin_ = acp->data_.pos_;
  return 1;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_capture_end(a_see_parser_t* acp)
{
  acp->data_.capture_end_ = acp->data_.pos_;
  return 1;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE unsigned int a_see_parser_yylen(const a_see_parser_t* acp)
{
  return acp->data_.capture_end_>=acp->data_.capture_begin_
    ? acp->data_.capture_end_-acp->data_.capture_begin_ : 0;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE const char* a_see_parser_yytext(a_see_parser_t* acp)
{
  unsigned int len = a_see_parser_yylen(acp);
  if(len) {
    if(len > acp->yytext_.capacity_) {
      a_see_parser_buffer_increase_capacity(&acp->yytext_,len);
    }
    memcpy(acp->yytext_.buffer_,acp->input_.buffer_+acp->data_.capture_begin_,
        acp->data_.capture_end_-acp->data_.capture_begin_);
  }
  acp->yytext_.buffer_[len]=0;
  return acp->yytext_.buffer_;
}

/* Tracing.  Produces a lot of output */
#ifdef A_SEE_PARSER_TRACE
#define A_SEE_PARSER_DEBUG if(1)
#define A_SEE_PARSER_TRACE_GRAMMAR(ACP_PTR,code)  ({ {code} 1; })
#define A_SEE_PARSER_TRACE_INFO(ACP_PTR,PREDICATE,STATUS) \
  fprintf(stderr,"%-8s %-30.30s %-20s :%.30s\n",STATUS,__func__,PREDICATE,(ACP_PTR)->input_.buffer_+(ACP_PTR)->data_.pos_)
#define A_SEE_PARSER_TRACE_INFO_ENTER(ACP_PTR,PREDICATE) A_SEE_PARSER_TRACE_INFO(ACP_PTR,PREDICATE,"")
#define A_SEE_PARSER_TRACE_INFO_SUCCESS(ACP_PTR,PREDICATE) A_SEE_PARSER_TRACE_INFO(ACP_PTR,PREDICATE,"Success ")
#define A_SEE_PARSER_TRACE_INFO_FAIL(ACP_PTR,PREDICATE) A_SEE_PARSER_TRACE_INFO(ACP_PTR,PREDICATE,"Fail ")
#else
#define A_SEE_PARSER_DEBUG if(0)
#define A_SEE_PARSER_TRACE_GRAMMAR(ACP_PTR,code) (1)
#define A_SEE_PARSER_TRACE_INFO_ENTER(ACP_PTR,PREDICATE)
#define A_SEE_PARSER_TRACE_INFO_SUCCESS(ACP_PTR,PREDICATE)
#define A_SEE_PARSER_TRACE_INFO_FAIL(ACP_PTR,PREDICATE)
#endif


A_SEE_PARSER_FUNCIION_ATTRIBUTE void a_see_parser_init(a_see_parser_t* acp,unsigned int yytext_buffer_initial_capacity, unsigned int input_buffer_initial_capacity)
{
  acp->input_.size_=0;
  acp->input_.no_more_data_=0;
  acp->data_.pos_ = 0;
  acp->data_.capture_begin_ = 0;
  acp->data_.capture_end_ = 0;
  acp->data_.line_ = 1;
  acp->data_.col_ = 1;
  acp->max_pos_=0;
  a_see_parser_buffer_init(&acp->yytext_,yytext_buffer_initial_capacity);
  a_see_parser_input_buffer_init(&acp->input_,input_buffer_initial_capacity);
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE void a_see_parser_destroy(a_see_parser_t* acp)
{
  a_see_parser_buffer_destroy(&acp->yytext_);
  a_see_parser_input_buffer_destroy(&acp->input_);
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_flush(a_see_parser_t* acp)
{
  memcpy(acp->input_.buffer_,acp->input_.buffer_+acp->data_.pos_,acp->input_.size_-acp->data_.pos_);
  acp->input_.size_ -= acp->data_.pos_;
  acp->input_.buffer_[acp->input_.size_]=0;
  acp->data_.pos_=0;
  acp->max_pos_=0;
  return 1;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_peek_chr(a_see_parser_t* acp)
{
  if(!acp->input_.no_more_data_ && acp->data_.pos_ == acp->input_.size_) {
    if(acp->input_.size_ == acp->input_.capacity_)
      a_see_parser_input_buffer_increase_capacity(&acp->input_,acp->input_.capacity_<<1);
    unsigned int ninput;
#ifdef A_SEE_PARSER_LOCAL
    A_SEE_PARSER_INPUT(acp,acp->input_.buffer_+acp->input_.size_,ninput,acp->input_.capacity_-acp->input_.size_);
#else
    A_SEE_PARSER_INPUT(acp->input_.buffer_+acp->input_.size_,ninput,acp->input_.capacity_-acp->input_.size_);
#endif
    acp->input_.no_more_data_= ninput == 0;
    acp->input_.buffer_[acp->input_.size_+=ninput]=0;
  }
  return acp->input_.buffer_[acp->data_.pos_];
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_next_chr(a_see_parser_t* acp)
{
  int c = a_see_parser_peek_chr(acp);
  if(acp->data_.pos_ > acp->max_pos_)
    acp->max_pos_=acp->data_.pos_;
  if(c) {
    acp->data_.pos_++;
#ifdef __WHAT_EVER_TO_USE_FOR_MAC__
    if(c == '\r')
#else
    if(c == '\n')
#endif
    {
      acp->data_.line_++;
      acp->data_.col_=1;
    } else
      acp->data_.col_++;
  }
  return c;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_do_nothing_toupper(int c) { return c; }
A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_literal(a_see_parser_t* acp,const char*str,int(*upcase_fun)(int))
{
  int rc=0;
  if(upcase_fun(*str) == upcase_fun(a_see_parser_peek_chr(acp))) {
    A_SEE_PARSER_SAVE_STATE(acp);
    int c;
    for(str++,a_see_parser_next_chr(acp);*str;str++) {
      if(!(c=a_see_parser_next_chr(acp)) || upcase_fun(c) != upcase_fun(*str))
        break;
    }
    if(*str == 0)
      rc=1;
    else
      A_SEE_PARSER_RESTORE_STATE(acp);
  }
  return rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_char_set(a_see_parser_t* acp,const char*char_set)
{
  int rc=0;
  int c = a_see_parser_peek_chr(acp);
  if(c) {
    int fc;
    int reverse = 0;
    if(*char_set == '^') {
      reverse = 1;
      char_set++;
    }
    for(;*char_set;char_set++) {
      fc=*char_set;
      if(fc == '\\' && *(char_set+1)) {
        fc = *(++char_set);
      } else if(fc == '.') {
        rc = 1;
        break;
      }
      if(*(char_set+1)== '-' && *(char_set+2)) {
        char_set+=2;
        if(fc <= c && c <= *char_set) {
          rc = 1;
          break;
        }
      } else {
        if(c == fc) {
          rc=1;
          break;
        }
      }
    }
    if(reverse)
      rc = !rc;
    if(rc && !reverse)
      a_see_parser_next_chr(acp);
  }
  return rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_next_chr_is(a_see_parser_t* acp,int ch)
{
  return (a_see_parser_peek_chr(acp) == ch) && a_see_parser_next_chr(acp);
}

/* sequence of predicates and/or other sequences */
#define REAL_A_SEE_PARSER_SIMPLE_SEQUENCE(ACP_PTR,seq) \
      ({\
        A_SEE_PARSER_SAVE_STATE(ACP_PTR); \
        A_SEE_PARSER_TRACE_INFO_ENTER(ACP_PTR,"SIMPLE_SEQUENCE"); \
        int __Rc__ = seq; \
        if(__Rc__) { \
          A_SEE_PARSER_TRACE_INFO_SUCCESS(ACP_PTR,"SIMPLE_SEQUENCE"); \
        } \
        else {\
          A_SEE_PARSER_TRACE_INFO_FAIL(ACP_PTR,"SIMPLE_SEQUENCE"); \
          A_SEE_PARSER_RESTORE_STATE(ACP_PTR); \
        } \
        __Rc__; \
       })

/* Same as simple sequence with action and cleanup */
#define REAL_A_SEE_PARSER_SEQUENCE(ACP_PTR,seq,action,cleanup) \
  ({ \
    A_SEE_PARSER_SAVE_STATE(ACP_PTR); \
    A_SEE_PARSER_TRACE_INFO_ENTER(ACP_PTR,"SEQUENCE"); \
    int __rC__ = seq; \
    if(__rC__) { \
      { action } \
      A_SEE_PARSER_TRACE_INFO_SUCCESS(ACP_PTR,"SEQUENCE"); \
    } else { \
      { cleanup } \
      A_SEE_PARSER_TRACE_INFO_FAIL(ACP_PTR,"SEQUENCE"); \
      A_SEE_PARSER_RESTORE_STATE(ACP_PTR); \
    } \
    __rC__; \
  })

/* PEG equivalent -- (sequence)* */
#define REAL_A_SEE_PARSER_ZERO_OR_MORE(ACP_PTR,seq,action,cleanup) \
  ({ \
    do { \
      A_SEE_PARSER_SAVE_STATE(ACP_PTR); \
      A_SEE_PARSER_TRACE_INFO_ENTER(ACP_PTR,"ZERO_OR_MORE");\
      if( seq ) { \
        { action } \
        A_SEE_PARSER_TRACE_INFO_SUCCESS(ACP_PTR,"ZERO_OR_MORE");\
       } else { \
        { cleanup} \
        A_SEE_PARSER_TRACE_INFO_FAIL(ACP_PTR,"ZERO_OR_MORE"); \
        A_SEE_PARSER_RESTORE_STATE(ACP_PTR); break; \
      } \
    } while(1); \
    1; \
  })
/* PEG equivalent -- (sequence)+ */
#define REAL_A_SEE_PARSER_ONE_OR_MORE(ACP_PTR,seq,action,cleanup) \
  ({\
    int __cOuNt__=0;\
    do { \
      A_SEE_PARSER_SAVE_STATE(ACP_PTR); \
      A_SEE_PARSER_TRACE_INFO_ENTER(ACP_PTR,"ONE_OR_MORE");\
      if(seq) { \
        __cOuNt__++; \
        A_SEE_PARSER_TRACE_INFO_SUCCESS(ACP_PTR,"ONE_OR_MORE");\
        { action } \
      } else { \
        \
       { cleanup } A_SEE_PARSER_TRACE_INFO_FAIL(ACP_PTR,"ONE_OR_MORE"); \
         A_SEE_PARSER_RESTORE_STATE(ACP_PTR); break; \
      } \
    } while(1); \
    __cOuNt__>0; \
  })

/* PEG equivalent -- (sequence)? */
#define REAL_A_SEE_PARSER_OPTIONAL(ACP_PTR,seq,action,cleanup) \
  ({ \
    A_SEE_PARSER_SAVE_STATE(ACP_PTR); \
    A_SEE_PARSER_TRACE_INFO_ENTER(ACP_PTR,"OPTIONAL");\
    if(seq) { \
      { action } \
      A_SEE_PARSER_TRACE_INFO_SUCCESS(ACP_PTR,"OPTIONAL"); \
    } else { \
      { cleanup } \
      A_SEE_PARSER_TRACE_INFO_FAIL(ACP_PTR,"OPTIONAL"); \
      A_SEE_PARSER_RESTORE_STATE(ACP_PTR); \
    } \
    1; \
  })

/* PEG equivalent -- &(sequenct) */
#define REAL_A_SEE_PARSER_NON_CONSUMING(ACP_PTR,seq) \
  ({ \
    A_SEE_PARSER_SAVE_STATE(ACP_PTR); \
    A_SEE_PARSER_TRACE_INFO_ENTER(ACP_PTR,"NON_CONSUMING");\
    int __rC__=seq;\
    A_SEE_PARSER_DEBUG { \
      if(__rC__) \
        A_SEE_PARSER_TRACE_INFO_SUCCESS(ACP_PTR,"NON_CONSUMING"); \
      else \
        A_SEE_PARSER_TRACE_INFO_FAIL(ACP_PTR,"NON_CONSUMING"); \
    } \
    A_SEE_PARSER_RESTORE_STATE(ACP_PTR);\
    __rC__;\
  })

#ifdef A_SEE_PARSER_LOCAL
#define A_SEE_PARSER_INIT(A_SEE_PARSER_PTR) \
    a_see_parser_init(A_SEE_PARSER_PTR,A_SEE_PARSER_CAPTURE_BUFFER_SIZE,A_SEE_PARSER_INPUT_BUFFER_SIZE)
#define A_SEE_PARSER_FLUSH(A_SEE_PARSER_PTR) a_see_parser_flush(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_DESTROY(A_SEE_PARSER_PTR) a_see_parser_destroy(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_PEEK_CHR(A_SEE_PARSER_PTR) a_see_parser_peek_chr(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_NEXT_CHR(A_SEE_PARSER_PTR) a_see_parser_next_chr(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_ANY(A_SEE_PARSER_PTR) a_see_parser_next_chr(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_NEXT_CHR_IS(A_SEE_PARSER_PTR,CH) a_see_parser_next_chr_is(A_SEE_PARSER_PTR,CH)
#define A_SEE_PARSER_NEXT_CHR_IS_NOT(A_SEE_PARSER_PTR,CH) (a_see_parser_peek_chr(A_SEE_PARSER_PTR) != CH)
#define A_SEE_PARSER_LITERAL(A_SEE_PARSER_PTR,STR) a_see_parser_literal(A_SEE_PARSER_PTR,STR,a_see_parser_do_nothing_toupper)
#define A_SEE_PARSER_LITERAL_ICASE(A_SEE_PARSER_PTR,STR) a_see_parser_literal(A_SEE_PARSER_PTR,STR,toupper)
#define A_SEE_PARSER_CHAR_SET(A_SEE_PARSER_PTR,STR) a_see_parser_char_set(A_SEE_PARSER_PTR,STR)
#define A_SEE_PARSER_SIMPLE_SEQUENCE(A_SEE_PARSER_PTR,SEQ) REAL_A_SEE_PARSER_SIMPLE_SEQUENCE(A_SEE_PARSER_PTR,SEQ)
#define A_SEE_PARSER_SEQUENCE(A_SEE_PARSER_PTR,SEQ,ACTION,CLEANUP)\
    REAL_A_SEE_PARSER_SEQUENCE(A_SEE_PARSER_PTR,SEQ,ACTION,CLEANUP)
#define A_SEE_PARSER_ZERO_OR_MORE(A_SEE_PARSER_PTR,SEQ,ACTION,CLEANUP) \
    REAL_A_SEE_PARSER_ZERO_OR_MORE(A_SEE_PARSER_PTR,SEQ,ACTION,CLEANUP)
#define A_SEE_PARSER_ONE_OR_MORE(A_SEE_PARSER_PTR,SEQ,ACTION,CLEANUP) \
    REAL_A_SEE_PARSER_ONE_OR_MORE(A_SEE_PARSER_PTR,SEQ,ACTION,CLEANUP)
#define A_SEE_PARSER_OPTIONAL(A_SEE_PARSER_PTR,SEQ,ACTION,CLEANUP) \
    REAL_A_SEE_PARSER_OPTIONAL(A_SEE_PARSER_PTR,SEQ,ACTION,CLEANUP)
#define A_SEE_PARSER_NON_CONSUMING(A_SEE_PARSER_PTR,SEQ) \
    REAL_A_SEE_PARSER_NON_CONSUMING(A_SEE_PARSER_PTR,SEQ)
#define A_SEE_PARSER_NOT(A_SEE_PARSER_PTR,SEQ) A_SEE_PARSER_NON_CONSUMING(A_SEE_PARSER_PTR,!(SEQ))
#define A_SEE_PARSER_CAPTURE_BEGIN(A_SEE_PARSER_PTR) a_see_parser_capture_begin(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_CAPTURE_END(A_SEE_PARSER_PTR) a_see_parser_capture_end(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_CAPTURE(A_SEE_PARSER_PTR,SEQ,ACTION) a_see_parser_capture_begin(A_SEE_PARSER_PTR) \
    && (SEQ) && a_see_parser_capture_end(A_SEE_PARSER_PTR) && ({ ACTION 1;})
#define A_SEE_PARSER_YYTEXT(A_SEE_PARSER_PTR) a_see_parser_yytext(A_SEE_PARSER_PTR)
#else
a_see_parser_t __acp__;
a_see_parser_t* __acp_ptr__=&__acp__;
#define A_SEE_PARSER_INIT \
    a_see_parser_init(__acp_ptr__,A_SEE_PARSER_CAPTURE_BUFFER_SIZE,A_SEE_PARSER_INPUT_BUFFER_SIZE)
#define A_SEE_PARSER_FLUSH a_see_parser_flush(__acp_ptr__)
#define A_SEE_PARSER_DESTROY a_see_parser_destroy(__acp_ptr__)
#define A_SEE_PARSER_PEEK_CHR a_see_parser_peek_chr(__acp_ptr__)
#define A_SEE_PARSER_NEXT_CHR a_see_parser_next_chr(__acp_ptr__)
#define A_SEE_PARSER_ANY a_see_parser_next_chr(__acp_ptr__)
#define A_SEE_PARSER_NEXT_CHR_IS(CH) a_see_parser_next_chr_is(__acp_ptr__,CH)
#define A_SEE_PARSER_NEXT_CHR_IS_NOT(CH) (a_see_parser_peek_chr(__acp_ptr__) != CH)
#define A_SEE_PARSER_LITERAL(STR) a_see_parser_literal(__acp_ptr__,STR,a_see_parser_do_nothing_toupper)
#define A_SEE_PARSER_LITERAL_ICASE(STR) a_see_parser_literal(__acp_ptr__,STR,toupper)
#define A_SEE_PARSER_CHAR_SET(STR) a_see_parser_char_set(__acp_ptr__,STR)
#define A_SEE_PARSER_SIMPLE_SEQUENCE(SEQ) REAL_A_SEE_PARSER_SIMPLE_SEQUENCE(__acp_ptr__,SEQ)
#define A_SEE_PARSER_SEQUENCE(SEQ,ACTION,CLEANUP)\
    REAL_A_SEE_PARSER_SEQUENCE(__acp_ptr__,SEQ,ACTION,CLEANUP)
#define A_SEE_PARSER_ZERO_OR_MORE(SEQ,ACTION,CLEANUP) \
    REAL_A_SEE_PARSER_ZERO_OR_MORE(__acp_ptr__,SEQ,ACTION,CLEANUP)
#define A_SEE_PARSER_ONE_OR_MORE(SEQ,ACTION,CLEANUP) \
    REAL_A_SEE_PARSER_ONE_OR_MORE(__acp_ptr__,SEQ,ACTION,CLEANUP)
#define A_SEE_PARSER_OPTIONAL(SEQ,ACTION,CLEANUP) \
    REAL_A_SEE_PARSER_OPTIONAL(__acp_ptr__,SEQ,ACTION,CLEANUP)
#define A_SEE_PARSER_NON_CONSUMING(SEQ) \
    REAL_A_SEE_PARSER_NON_CONSUMING(__acp_ptr__,SEQ)
#define A_SEE_PARSER_NOT(SEQ) A_SEE_PARSER_NON_CONSUMING(!(SEQ))
#define A_SEE_PARSER_CAPTURE_BEGIN a_see_parser_capture_begin(__acp_ptr__)
#define A_SEE_PARSER_CAPTURE_END a_see_parser_capture_end(__acp_ptr__)
#define A_SEE_PARSER_CAPTURE(SEQ,ACTION) a_see_parser_capture_begin(__acp_ptr__) \
    && (SEQ) && a_see_parser_capture_end(__acp_ptr__) && ({ ACTION 1;})
#define A_SEE_PARSER_YYTEXT a_see_parser_yytext(__acp_ptr__)
#endif
/*
  Note that A_SEE_PARSER_NOT is does not consume input.  Input may be consumed if using C "!" operator.
  Outside of a SEQUENCE, use A_SEE_PARSER_NOT rather than "!"

  Example:
    !KEYWORD1 && !KEYWORD2 && IDENT

    if KEYWORD2 is true (thus !KEYWORD2 is false) the sequence fails but the parser IS NOT returned to original state

  However,
    A_SEE_PARSER_NOT(KEYWORD1) && A_SEE_PARSER_NOT(KEYWORD2) && IDENT

    if KEYWORD2 is true (thus !KEYWORD2 is false) the sequence fails and the parser IS returned to original state

  Better,

    SIMPLE_SEQUENCE(!KEYWORD1 && !KEYWORD2 && IDENT)

    if KEYWORD2 is true (thus !KEYWORD2 is false) the sequence fails and the parser IS returned to original state

*/

#ifdef A_SEE_PARSER_USE_SHORT_FORM
#define FLUSH  A_SEE_PARSER_FLUSH
#define DESTROY A_SEE_PARSER_DESTROY
#define PEEK_CHR  A_SEE_PARSER_PEEK_CHR
#define NEXT_CHR  A_SEE_PARSER_NEXT_CHR
#define ANY  A_SEE_PARSER_ANY
#define NEXT_CHR_IS A_SEE_PARSER_NEXT_CHR_IS
#define NEXT_CHR_IS_NOT A_SEE_PARSER_NEXT_CHR_IS_NOT
#define LITERAL A_SEE_PARSER_LITERAL
#define LITERAL_ICASE A_SEE_PARSER_LITERAL_ICASE
#define CHAR_SET A_SEE_PARSER_CHAR_SET
#define SIMPLE_SEQUENCE A_SEE_PARSER_SIMPLE_SEQUENCE
#define SEQUENCE A_SEE_PARSER_SEQUENCE
#define ZERO_OR_MORE A_SEE_PARSER_ZERO_OR_MORE
#define ONE_OR_MORE A_SEE_PARSER_ONE_OR_MORE
#define OPTIONAL A_SEE_PARSER_OPTIONAL
#define NON_CONSUMING A_SEE_PARSER_NON_CONSUMING
#define NOT A_SEE_PARSER_NOT
#define CAPTURE_BEGIN A_SEE_PARSER_CAPTURE_BEGIN
#define CAPTURE_END A_SEE_PARSER_CAPTURE_END
#define CAPTURE A_SEE_PARSER_CAPTURE
#define YYTEXT A_SEE_PARSER_YYTEXT

#endif  // A_SEE_PARSER_USE_SHORT_FORM for  non terminals


#ifdef A_SEE_PARSER_USE_BUILTIN_TERMINALS

// Eats whitespace -- normally " \t" or " \t\r\n"
A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_whitespace(a_see_parser_t* acp,const char* ws_chars)
{
  int c,rc=0;
  if((c=a_see_parser_peek_chr(acp)) && strchr(ws_chars,c)) {
    rc=1;
    for(a_see_parser_next_chr(acp);
        (c=a_see_parser_peek_chr(acp)) && strchr(ws_chars,c);
        a_see_parser_next_chr(acp));
  }
  return rc;
}

// End of line -- '\r' || '\n' || '\r\n'
A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_eol(a_see_parser_t* acp)
{
  int rc = a_see_parser_peek_chr(acp) == '\r' && a_see_parser_next_chr(acp);
  return ( a_see_parser_peek_chr(acp) == '\n' && a_see_parser_next_chr(acp)) || rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_binary_integer(a_see_parser_t* acp)
{
  int rc=0;
  if(a_see_parser_peek_chr(acp) == '0') {
    A_SEE_PARSER_SAVE_STATE(acp);
    a_see_parser_next_chr(acp);
    if(toupper(a_see_parser_peek_chr(acp)) == 'B') {
      a_see_parser_next_chr(acp);
      int c;
      if((c=a_see_parser_peek_chr(acp)) == '0' || c == '1' ) {
        rc=1;
        for(a_see_parser_next_chr(acp);
            (c=a_see_parser_peek_chr(acp)) == '0' || c == '1';
            a_see_parser_next_chr(acp));
      }
    }
    if(!rc)
      A_SEE_PARSER_RESTORE_STATE(acp);
  }
  return rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_octal_integer(a_see_parser_t* acp)
{
  int rc=0,c;
  if(a_see_parser_peek_chr(acp) == '0') {
    for(a_see_parser_next_chr(acp);
    (c=a_see_parser_peek_chr(acp)) >= '0' && c <= '7';
    a_see_parser_next_chr(acp));
    rc=1;
  }
  return rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_decimal_integer(a_see_parser_t* acp)
{
  int rc=0,c;
  if((c=a_see_parser_peek_chr(acp)) >= '1' && c <= '9') {
    for(a_see_parser_next_chr(acp);
      (c=a_see_parser_peek_chr(acp)) >= '0' && c <= '9';
      a_see_parser_next_chr(acp));
    rc=1;
  }
  return rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_hex_integer(a_see_parser_t* acp)
{
  int rc=0;
  if(a_see_parser_peek_chr(acp) == '0') {
    A_SEE_PARSER_SAVE_STATE(acp);
    a_see_parser_next_chr(acp);
    if(toupper(a_see_parser_peek_chr(acp)) == 'X') {
      a_see_parser_next_chr(acp);
      int c;
      if(((c=toupper(a_see_parser_peek_chr(acp))) >= '0' && c <= '9') || ( c >= 'A' && c <= 'F')) {
        rc=1;
        for(a_see_parser_next_chr(acp);
          ((c=toupper(a_see_parser_peek_chr(acp))) >= '0' && c <= '9') || ( c >= 'A' && c <= 'F');
          a_see_parser_next_chr(acp));
      }
    }
    if(!rc)
      A_SEE_PARSER_RESTORE_STATE(acp);
  }
  return rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_floating_point(a_see_parser_t* acp)
{
  int has_whole = 0;
  int has_decimal_point = 0;
  int has_decimal_digits=0;
  int has_exponent=0; // 0 - not specified, -1 -- e (or E) without digits, 1 -- valid
  int c,rc=0;
  A_SEE_PARSER_SAVE_STATE(acp);
  if((c=a_see_parser_peek_chr(acp)) >= '0' && c <= '9') {
    has_whole=1;
    for(a_see_parser_next_chr(acp);(c=a_see_parser_peek_chr(acp)) >= '0' && c <= '9';a_see_parser_next_chr(acp));
  }
  if (a_see_parser_peek_chr(acp) == '.') {
    has_decimal_point = 1;
    a_see_parser_next_chr(acp);
    if((c=a_see_parser_peek_chr(acp)) >= '0' && c <= '9') {
      has_decimal_digits=1;
      for(a_see_parser_next_chr(acp);(c=a_see_parser_peek_chr(acp)) >= '0' && c <= '9';a_see_parser_next_chr(acp));
    }
  }
  if(has_whole || has_decimal_digits ) {
    A_SEE_PARSER_SAVE_STATE(acp);
    if(toupper(a_see_parser_peek_chr(acp))=='E') {
      a_see_parser_next_chr(acp);
      if((c=a_see_parser_peek_chr(acp))=='+' || c == '-')
        a_see_parser_next_chr(acp);
      if((c=a_see_parser_peek_chr(acp)) >= '0' && c <= '9') {
        has_exponent=1;
        for(a_see_parser_next_chr(acp);(c=a_see_parser_peek_chr(acp)) >= '0' && c <= '9';a_see_parser_next_chr(acp));
      }
    }
    if(!has_exponent)
      A_SEE_PARSER_RESTORE_STATE(acp);
  }
  rc = ((has_whole && has_exponent)
        || (has_whole && has_decimal_point) || has_decimal_digits);
  if(!rc)
    A_SEE_PARSER_RESTORE_STATE(acp);
  return rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_identifier(a_see_parser_t* acp,const char* first,const char* all)
{
  int rc=0;
  if(a_see_parser_char_set(acp,first)) {
    rc=1;
    while(a_see_parser_char_set(acp,all));
  }
  return rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_quoted_string(a_see_parser_t* acp,int ch)
{
  (void)a_see_parser_quoted_string;
  int rc=0;
  if(a_see_parser_peek_chr(acp) == ch) {
    A_SEE_PARSER_SAVE_STATE(acp);
    int c;
    for(a_see_parser_next_chr(acp);(c=a_see_parser_peek_chr(acp));a_see_parser_next_chr(acp)) {
      if(c == '\\') a_see_parser_next_chr(acp);
      else if(c == ch) {
        a_see_parser_next_chr(acp);
        rc=1;
        break;
      }
    }
    if(!rc)
      A_SEE_PARSER_RESTORE_STATE(acp);
  }
  return rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_delimited_comment(a_see_parser_t* acp,const char* start,const char* end,int nested)
{
  int rc=0;
  A_SEE_PARSER_SAVE_STATE(acp);
  if(a_see_parser_literal(acp,start,a_see_parser_do_nothing_toupper)) {
    while(a_see_parser_peek_chr(acp)) {
      if(nested)
        a_see_parser_delimited_comment(acp,start,end,nested);
      if(a_see_parser_literal(acp,end,a_see_parser_do_nothing_toupper)) {
        rc =1;
        break;
      }
      a_see_parser_next_chr(acp);
    }
  }
  if(!rc)
    A_SEE_PARSER_RESTORE_STATE(acp);
  return rc;
}

A_SEE_PARSER_FUNCIION_ATTRIBUTE int a_see_parser_one_line_comment(a_see_parser_t* acp,const char* start)
{
  int rc=0;
  A_SEE_PARSER_SAVE_STATE(acp);
  /* if the last character in start if alpha numeric, then next character must be non alpha numeric */
  if(a_see_parser_literal(acp,start,toupper) && (!a_see_parser_peek_chr(acp) ||
        (isalpha(start[strlen(start)-1]) ? !isalpha(a_see_parser_peek_chr(acp)): 1))) {
    rc = 1;
    int c;
    while((c=a_see_parser_peek_chr(acp)) && (c!='\r' && c!= '\n'))
      a_see_parser_next_chr(acp);
  }
  if(!rc)
    A_SEE_PARSER_RESTORE_STATE(acp);
  return rc;
}

#ifdef A_SEE_PARSER_LOCAL

#define A_SEE_PARSER_WHITESPACE(A_SEE_PARSER_PTR,WS_CHRS) a_see_parser_whitespace(A_SEE_PARSER_PTR,WS_CHRS)
#define A_SEE_PARSER_EOL(A_SEE_PARSER_PTR) a_see_parser_eol(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_BINARY_INTEGER(A_SEE_PARSER_PTR) a_see_parser_binary_integer(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_OCTAL_INTEGER(A_SEE_PARSER_PTR) a_see_parser_octal_integer(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_DECIMAL_INTEGER(A_SEE_PARSER_PTR) a_see_parser_decimal_integer(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_HEX_INTEGER(A_SEE_PARSER_PTR) a_see_parser_hex_integer(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_FLOATING_POINT(A_SEE_PARSER_PTR) a_see_parser_floating_point(A_SEE_PARSER_PTR)
#define A_SEE_PARSER_IDENTIFIER(A_SEE_PARSER_PTR,F,R) a_see_parser_identifier(A_SEE_PARSER_PTR,F,R)
#define A_SEE_PARSER_SINGLE_QUOTED_STRING(A_SEE_PARSER_PTR) a_see_parser_quoted_string(A_SEE_PARSER_PTR,'\'')
#define A_SEE_PARSER_DOUBLE_QUOTED_STRING(A_SEE_PARSER_PTR) a_see_parser_quoted_string(A_SEE_PARSER_PTR,'"')
#define A_SEE_PARSER_QUOTED_STRING(A_SEE_PARSER_PTR) \
    ( A_SEE_PARSER_DOUBLE_QUOTED_STRING(A_SEE_PARSER_PTR) || A_SEE_PARSER_DOUBLE_QUOTED_STRING(A_SEE_PARSER_PTR))
#define A_SEE_PARSER_C_COMMENT(A_SEE_PARSER_PTR) a_see_parser_delimited_comment(A_SEE_PARSER_PTR,"/*","*/",0)
#define A_SEE_PARSER_NESTED_C_COMMENT(A_SEE_PARSER_PTR) a_see_parser_delimited_comment(A_SEE_PARSER_PTR,"/*","*/",1)
#define A_SEE_PARSER_PASCAL_COMMENT(A_SEE_PARSER_PTR) a_see_parser_delimited_comment(A_SEE_PARSER_PTR,"(*","*)",0)
#define A_SEE_PARSER_NESTED_PASCAL_COMMENT(A_SEE_PARSER_PTR) a_see_parser_delimited_comment(A_SEE_PARSER_PTR,"(*","*)",1)
#define A_SEE_PARSER_LUA_BLOCK_COMMENT(A_SEE_PARSER_PTR) a_see_parser_delimited_comment(A_SEE_PARSER_PTR,"--[[","]]",0)
#define A_SEE_PARSER_XML_COMMENT(A_SEE_PARSER_PTR) a_see_parser_delimited_comment(A_SEE_PARSER_PTR,"<!--","-->",0)
#define A_SEE_PARSER_CPP_COMMENT(A_SEE_PARSER_PTR) a_see_parser_one_line_comment(A_SEE_PARSER_PTR,"//")
#define A_SEE_PARSER_BASIC_COMMENT(A_SEE_PARSER_PTR)  a_see_parser_one_line_comment(A_SEE_PARSER_PTR,"rem")
#define A_SEE_PARSER_SHELL_COMMENT(A_SEE_PARSER_PTR)  a_see_parser_one_line_comment(A_SEE_PARSER_PTR,"#")
#define A_SEE_PARSER_RUBY_COMMENT(A_SEE_PARSER_PTR)  a_see_parser_one_line_comment(A_SEE_PARSER_PTR,"#")
#define A_SEE_PARSER_PERL_COMMENT(A_SEE_PARSER_PTR)  a_see_parser_one_line_comment(A_SEE_PARSER_PTR,"#")
#define A_SEE_PARSER_LUA_COMMENT(A_SEE_PARSER_PTR) a_see_parser_one_line_comment(A_SEE_PARSER_PTR,"--")

#else // A_SEE_PARSER_LOCAL for terminals

#define A_SEE_PARSER_WHITESPACE(WS_CHRS) a_see_parser_whitespace(__acp_ptr__,WS_CHRS)
#define A_SEE_PARSER_EOL a_see_parser_eol(__acp_ptr__)
#define A_SEE_PARSER_BINARY_INTEGER a_see_parser_binary_integer(__acp_ptr__)
#define A_SEE_PARSER_OCTAL_INTEGER a_see_parser_octal_integer(__acp_ptr__)
#define A_SEE_PARSER_DECIMAL_INTEGER a_see_parser_decimal_integer(__acp_ptr__)
#define A_SEE_PARSER_HEX_INTEGER a_see_parser_hex_integer(__acp_ptr__)
#define A_SEE_PARSER_FLOATING_POINT a_see_parser_floating_point(__acp_ptr__)
#define A_SEE_PARSER_IDENTIFIER(F,R) a_see_parser_identifier(__acp_ptr__,F,R)
#define A_SEE_PARSER_SINGLE_QUOTED_STRING a_see_parser_quoted_string(__acp_ptr__,'\'')
#define A_SEE_PARSER_DOUBLE_QUOTED_STRING a_see_parser_quoted_string(__acp_ptr__,'"')
#define A_SEE_PARSER_QUOTED_STRING \
    ( A_SEE_PARSER_DOUBLE_QUOTED_STRING || A_SEE_PARSER_DOUBLE_QUOTED_STRING)
#define A_SEE_PARSER_C_COMMENT a_see_parser_delimited_comment(__acp_ptr__,"/*","*/",0)
#define A_SEE_PARSER_NESTED_C_COMMENT a_see_parser_delimited_comment(__acp_ptr__,"/*","*/",1)
#define A_SEE_PARSER_PASCAL_COMMENT a_see_parser_delimited_comment(__acp_ptr__,"(*","*)",0)
#define A_SEE_PARSER_NESTED_PASCAL_COMMENT a_see_parser_delimited_comment(__acp_ptr__,"(*","*)",1)
#define A_SEE_PARSER_LUA_BLOCK_COMMENT a_see_parser_delimited_comment(__acp_ptr__,"--[[","]]",0)
#define A_SEE_PARSER_XML_COMMENT a_see_parser_delimited_comment(__acp_ptr__,"<!--","-->",0)
#define A_SEE_PARSER_CPP_COMMENT a_see_parser_one_line_comment(__acp_ptr__,"//")
#define A_SEE_PARSER_BASIC_COMMENT  a_see_parser_one_line_comment(__acp_ptr__,"rem")
#define A_SEE_PARSER_SHELL_COMMENT a_see_parser_one_line_comment(__acp_ptr__,"#")
#define A_SEE_PARSER_RUBY_COMMENT  a_see_parser_one_line_comment(__acp_ptr__,"#")
#define A_SEE_PARSER_PERL_COMMENT a_see_parser_one_line_comment(__acp_ptr__,"#")
#define A_SEE_PARSER_LUA_COMMENT a_see_parser_one_line_comment(__acp_ptr__,"--")

#endif // A_SEE_PARSER_LOCAL for terminals


#ifdef A_SEE_PARSER_USE_SHORT_FORM

#define WHITESPACE A_SEE_PARSER_WHITESPACE
#define EOL A_SEE_PARSER_EOL
#define BINARY_INTEGER A_SEE_PARSER_BINARY_INTEGER
#define OCTAL_INTEGER A_SEE_PARSER_OCTAL_INTEGER
#define DECIMAL_INTEGER A_SEE_PARSER_DECIMAL_INTEGER
#define HEX_INTEGER A_SEE_PARSER_HEX_INTEGER
#define FLOATING_POINT A_SEE_PARSER_FLOATING_POINT
#define IDENTIFIER A_SEE_PARSER_IDENTIFIER
#define SINGLE_QUOTED_STRING A_SEE_PARSER_SINGLE_QUOTED_STRING
#define DOUBLE_QUOTED_STRING A_SEE_PARSER_DOUBLE_QUOTED_STRING
#define QUOTED_STRING A_SEE_PARSER_QUOTED_STRING
#define C_COMMENT A_SEE_PARSER_C_COMMENT
#define NESTED_C_COMMENT A_SEE_PARSER_NESTED_C_COMMENT
#define PASCAL_COMMENT A_SEE_PARSER_PASCAL_COMMENT
#define NESTED_PASCAL_COMMENT A_SEE_PARSER_NESTED_PASCAL_COMMENT
#define LUA_BLOCK_COMMENT A_SEE_PARSER_LUA_BLOCK_COMMENT
#define XML_COMMENT A_SEE_PARSER_XML_COMMENT
#define CPP_COMMENT A_SEE_PARSER_CPP_COMMENT
#define BASIC_COMMENT A_SEE_PARSER_BASIC_COMMENT
#define SHELL_COMMENT A_SEE_PARSER_SHELL_COMMENT
#define RUBY_COMMENT A_SEE_PARSER_RUBY_COMMENT
#define PERL_COMMENT A_SEE_PARSER_PERL_COMMENT
#define LUA_COMMENT A_SEE_PARSER_LUA_COMMENT

#endif // A_SEE_PARSER_USE_SHORT_FORM for terminals

#endif // A_SEE_PARSER_USE_BUILTIN_TERMINALS

#ifdef __cplusplus
}
#endif

#endif // __A_SEE_PARSER_INCLUDED__
