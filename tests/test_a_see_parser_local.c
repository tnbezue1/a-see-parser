/*
		Copyright (C) 2021  by Terry N Bezue

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#define A_SEE_PARSER_LOCAL
#define A_SEE_PARSER_USE_BUILTIN_TERMINALS
const char* input_buffer;
unsigned int input_buffer_pos=0;

#define A_SEE_PARSER_INPUT(ACP,buf,result,max_size) \
  {\
    result = 0; \
    if(input_buffer[input_buffer_pos]) { \
      int l = strlen(input_buffer+input_buffer_pos); \
      result = l > max_size ? max_size : l; \
      memcpy(buf,input_buffer+input_buffer_pos,result); \
      input_buffer_pos+= result; \
    } \
  }

#define SET_INPUT_BUFFER(BUF) \
  { \
    input_buffer = BUF; \
    input_buffer_pos =0; \
  }

#include <a_see_parser/a_see_parser.h>
#include "test_harness.h"

// How to reset parser.  Really just for testing.
#define A_SEE_PARSER_RESET \
  { \
    myacp.input_.size_=0; \
    myacp.input_.no_more_data_=0; \
    myacp.data_.pos_ = 0; \
    myacp.data_.capture_begin_ = 0; \
    myacp.data_.capture_end_ = 0; \
    myacp.data_.line_ = 1; \
    myacp.data_.col_ = 1; \
  }



void test_init()
{
}
const char* not[] = { " not ", " "};
typedef struct {
  const char* str;
  int result;
  int next_chr;
} test_data_t;

void test_init_destroy()
{
  TESTCASE("Init");
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  TEST("Pos is zero",myacp.data_.pos_==0);
  TEST("Capture buffer capacity is 32",myacp.yytext_.capacity_==32);
  TEST("Input buffer is 1024",myacp.input_.capacity_==1024);
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_buffer()
{
  input_buffer="abcdefghijklmnopqrstuvwxyz";
  input_buffer_pos=0;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  unsigned pos = 1;
  while(A_SEE_PARSER_NEXT_CHR(&myacp)) {
    if(pos > 7) {
      A_SEE_PARSER_FLUSH(&myacp);
      pos=0;
    }
    pos++;
  }

  printf("Final: X%sX",myacp.input_.buffer_);
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_peek_next_chr()
{
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  TESTCASE("PEEK_CHR and NEXT_CHR");
  input_buffer="abcdefghijklmnopqrstuvwxyz";
  input_buffer_pos=0;
  char msg[128];
  // 0 -- next_chr this many times and ...
  // 1 -- ... peek will be this char
  int test_data[][2] = { { 0, 'a'}, { 4, 'e' }, {8,'m'}, {10,'w'}, { -1, -1}};
  int i;
  for(i=0;test_data[i][0] != -1;i++) {
    int j;
    for(j=0;j<test_data[i][0];j++)
      A_SEE_PARSER_NEXT_CHR(&myacp);
    sprintf(msg,"After skiping %d characters, peek char is '%c'",test_data[i][0],test_data[i][1]);
    TEST(msg,A_SEE_PARSER_PEEK_CHR(&myacp) == test_data[i][1]);
  }
  while(A_SEE_PARSER_NEXT_CHR(&myacp));
  unsigned int pos = myacp.data_.pos_;
  A_SEE_PARSER_NEXT_CHR(&myacp);
  TEST("When at end of input, next_chr returns 0",A_SEE_PARSER_NEXT_CHR(&myacp)==0);
  TEST("When at end of input, next_chr does not advance position",pos==myacp.data_.pos_);
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_next_chr_is()
{
  TESTCASE("Next char is");
  input_buffer="abcdef";
  input_buffer_pos=0;
  char msg[128];
  struct {
    int ch;
    int expected_result;
  } *itest_data,test_data[] =
  {
    {'x',0},
    {'b',1},
    {'z',0},
    {'d',1},
    {'e',1},
    {'m',0},
    {-1,-1}
  };
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->ch!=-1;itest_data++) {
    sprintf(msg,"NEXT_CHR_IS%s'%c'",not[itest_data->expected_result],itest_data->ch);
    TEST(msg,A_SEE_PARSER_NEXT_CHR_IS(&myacp,itest_data->ch) == itest_data->expected_result);
    if(!itest_data->expected_result)
      A_SEE_PARSER_NEXT_CHR(&myacp);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}


#define FLIP(a) (a ? 0 : 1)
void test_next_chr_is_not()
{
  TESTCASE("Next char is not");
  input_buffer="abcdef";
  input_buffer_pos=0;
  char msg[128];
  struct {
    int ch;
    int expected_result;
  } *itest_data,test_data[] =
  {
    {'x',1},
    {'b',0},
    {'z',1},
    {'d',0},
    {'e',0},
    {'m',1},
    {-1,-1}
  };
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->ch!=-1;itest_data++) {
    sprintf(msg,"NEXT_CHR_IS_NOT%s'%c'",not[FLIP(itest_data->expected_result)],itest_data->ch);
    TEST(msg,A_SEE_PARSER_NEXT_CHR_IS_NOT(&myacp,itest_data->ch) == itest_data->expected_result);
    A_SEE_PARSER_NEXT_CHR(&myacp);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_whitespace()
{
  TESTCASE("WHITESPACE");
  input_buffer="abc    def  \t\t  ghi\njkl \n\n\t\t     mno";
  input_buffer_pos=0;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  // 0 -- number of whitespace characters to expect
  // 1 -- next character after spacing
  int test_data[][2] =
  {
    { 0, 'a'},
    { 4, 'd',},
    { 6, 'g'},
    { 1, 'j'},
    { 10,'m'},
    { -1, 0}
  };
  char msg[128];
  int i;
  for(i=0;test_data[i][0] != -1; i++) {
    unsigned int pos = myacp.data_.pos_;
    sprintf(msg,"%d whitespace chars",test_data[i][0]);
    A_SEE_PARSER_WHITESPACE(&myacp," \r\n\t");
    TEST(msg,test_data[i][0] == myacp.data_.pos_-pos);
    sprintf(msg,"Next char is '%c'",test_data[i][1]);
    TEST(msg,A_SEE_PARSER_NEXT_CHR(&myacp) == test_data[i][1]);
    for(;!isspace(A_SEE_PARSER_PEEK_CHR(&myacp)) && A_SEE_PARSER_PEEK_CHR(&myacp)!=0;A_SEE_PARSER_NEXT_CHR(&myacp));
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_eol()
{
  TESTCASE("EOL");
  input_buffer="\r\n \r \n";
  input_buffer_pos=0;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  // 0 -- character that follows the eol string
  const char* test_data[] = { "Windows/Dos", "MAC", "Linux/Unix"};
  int i;
  char msg[128];
  for(i=0;i<3;i++) {
    sprintf(msg,"Matches %s EOL",test_data[i]);
    TEST(msg,A_SEE_PARSER_EOL(&myacp));
    A_SEE_PARSER_NEXT_CHR(&myacp);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_literal()
{
  TESTCASE("Literal");
  input_buffer="Apple apple OrAnge orangE";
  input_buffer_pos=0;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  struct {
    const char* str;
    int match;
  } *itest_data,test_data[] =
  {
    {"apple",0},
    {"Apple",1},
    {"apple",1},
    {"banana",0},
    {"OrAnge",1},
    {"orange",0},
    {"orangE",1},
    {"orangE",0},
    {NULL,0}
  };
  char msg[128];
  for(itest_data=test_data;itest_data->str;itest_data++) {
    sprintf(msg,"'%s' should%sbe found",itest_data->str,not[itest_data->match]);
    TEST(msg,A_SEE_PARSER_LITERAL(&myacp,itest_data->str) == itest_data->match);
    A_SEE_PARSER_WHITESPACE(&myacp," ");
  }
  TEST("At end of input",!A_SEE_PARSER_ANY(&myacp));
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_char_set()
{
  TESTCASE("Character set");
  input_buffer="-12 12 i12 Kay +54 33 -23";
  input_buffer_pos=0;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  struct {
    const char* char_set;
    int matches;
  } *itest_data,test_data[] = {
    { "-a-zA-Z0-9",1 },
    { "-a-zA-Z0-9",1 },
    { "aBcDeFgHiJkLmNoPqRsTuVwXyZ",1 },
    { "-a-zA-Z0-9",1 },
    { "-a-zA-Z0-9",0 },
    { "aBcDeFgHiJkLmNoPqRsTuVwXyZ",0 },
    { "a-zA-Z0-9-",1},
    { NULL, 0}
  };
  char msg[128];
  int c;
  for(itest_data=test_data;itest_data->char_set;itest_data++) {
    sprintf(msg,"'%s' is%sin set",itest_data->char_set,not[itest_data->matches]);
    TEST(msg,A_SEE_PARSER_CHAR_SET(&myacp,itest_data->char_set)==itest_data->matches);
    for(;(c=A_SEE_PARSER_PEEK_CHR(&myacp)) && !isspace(c);A_SEE_PARSER_NEXT_CHR(&myacp));
    A_SEE_PARSER_WHITESPACE(&myacp," ");
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_not_char_set()
{
  TESTCASE("Not in character set");
  input_buffer="12 12 i12 Kay +54 33";
  input_buffer_pos=0;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  struct {
    const char* char_set;
    int matches;
  } *itest_data,test_data[] = {
    { "^A-Z",1 },
    { "^0-9",0 },
    { "^aBcDeFgHiJkLmNoPqRsTuVwXyZ",0 },
    { "^-a-zA-Z0-9",0 },
    { "^-a-zA-Z0-9",1 },
    { "^aBcDeFgHiJkLmNoPqRsTuVwXyZ",1 },
    { NULL, 0}
  };
  char msg[128];
  int c;
  for(itest_data=test_data;itest_data->char_set;itest_data++) {
    sprintf(msg,"'%s' is%sin set",itest_data->char_set,not[itest_data->matches]);
    TEST(msg,A_SEE_PARSER_CHAR_SET(&myacp,itest_data->char_set)==itest_data->matches);
    for(;(c=A_SEE_PARSER_PEEK_CHR(&myacp)) && !isspace(c);A_SEE_PARSER_NEXT_CHR(&myacp));
    A_SEE_PARSER_WHITESPACE(&myacp," ");
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_binary_integer()
{
  TESTCASE("Binary Integer");
  input_buffer="0b10 b10 0b011000100111000101 0b011 0b021 0b";
  input_buffer_pos=0;
  int test_data[] = { 1, 0, 1, 1, 1, 0 , -1 };
  int *itest_data;
  char msg[256];
  int c;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;*itest_data!=-1;itest_data++) {
    sprintf(msg,"Binary Integer should%sbe found",not[*itest_data]);
    TEST(msg,A_SEE_PARSER_BINARY_INTEGER(&myacp) == *itest_data);
    for(;(c=A_SEE_PARSER_PEEK_CHR(&myacp)) && !isspace(c);A_SEE_PARSER_NEXT_CHR(&myacp));
    A_SEE_PARSER_WHITESPACE(&myacp," ");
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_octal_integer()
{
  TESTCASE("Octal Integer");
  input_buffer="0123 01234567123213124123 01238 123";
  input_buffer_pos=0;
  int test_data[] = { 1, 1, 1, 0 , -1 };
  int *itest_data;
  char msg[256];
  int c;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;*itest_data!=-1;itest_data++) {
    sprintf(msg,"Octal Integer should%sbe found",not[*itest_data]);
    TEST(msg,A_SEE_PARSER_OCTAL_INTEGER(&myacp) == *itest_data);
    for(;(c=A_SEE_PARSER_PEEK_CHR(&myacp)) && !isspace(c);A_SEE_PARSER_NEXT_CHR(&myacp));
    A_SEE_PARSER_WHITESPACE(&myacp," ");
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_decimal_integer()
{
  TESTCASE("Integer");
  input_buffer="0123 x32 1234 232 987623345828458697243 123a 0923";
  input_buffer_pos=0;
  int test_data[] = { 0, 0, 1, 1, 1, 1, 0, -1};
  int* itest_data;
  char msg[64];
  int c;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;*itest_data!=-1;itest_data++) {
    sprintf(msg,"Integer should%sbe found",not[*itest_data]);
    TEST(msg,A_SEE_PARSER_DECIMAL_INTEGER(&myacp) == *itest_data);
    for(;(c=A_SEE_PARSER_PEEK_CHR(&myacp)) && !isspace(c);A_SEE_PARSER_NEXT_CHR(&myacp));
    A_SEE_PARSER_WHITESPACE(&myacp," ");
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_hex_integer()
{
  TESTCASE("Hex Integer");
  input_buffer="0x123 x32 1234 0x12345678123213124123 0xfFfF 0x0 0x123G 0xG123 0x ";
  input_buffer_pos=0;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  struct {
    int result; //
    int next_chr; // the character after test
  } *itest_data,test_data[] =
  {
    {1,' '},
    {0,'x'},
    {0,'1',},
    {1,' ',},
    {1,' ',},
    {1,' ',},
    {1,'G'},
    {0,'0'},
    {0,'0'},
    {-1,0},
  };
  char msg[128];
  int c;
  for(itest_data=test_data;itest_data->result!=-1;itest_data++) {
    sprintf(msg,"Hex Integer should%sbe found",not[itest_data->result]);
    TEST(msg,A_SEE_PARSER_HEX_INTEGER(&myacp) == itest_data->result);
    TEST("Valid characters consumed",itest_data->next_chr == A_SEE_PARSER_PEEK_CHR(&myacp));
    for(;(c=A_SEE_PARSER_PEEK_CHR(&myacp)) && !isspace(c);A_SEE_PARSER_NEXT_CHR(&myacp));
    A_SEE_PARSER_WHITESPACE(&myacp," ");
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_floating_point()
{
  TESTCASE("Floating point");
  input_buffer="123.0 1234567. 123 123e 123.e 123e- 123e1 123e1x .0 ";
  input_buffer_pos=0;
  struct {
    int result;
    int next_chr;
  } *itest_data,test_data[] = {
    {1,' '},
    {1,' '},
    {0,'1'},
    {0,'1',},
    {1,'e'},
    {0,'1'},
    {1,' '},
    {1,'x'},
    {1,' '},
    {-1,0},
  };
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  char msg[256];
  int c;
  for(itest_data=test_data;itest_data->result!=-1;itest_data++) {
    sprintf(msg,"Floating poing number should%sbe found",not[itest_data->result]);
    TEST(msg,A_SEE_PARSER_FLOATING_POINT(&myacp) == itest_data->result);
    TEST("Valid characters consumed",itest_data->next_chr == A_SEE_PARSER_PEEK_CHR(&myacp));
    for(;(c=A_SEE_PARSER_PEEK_CHR(&myacp)) && !isspace(c);A_SEE_PARSER_NEXT_CHR(&myacp));
    A_SEE_PARSER_WHITESPACE(&myacp," ");
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_identifier()
{
  TESTCASE("Identifier");
  input_buffer="ident &b6  1rabc rb&123 ";
  input_buffer_pos=0;
  struct {
    int result;
    int next_chr;
  } *itest_data,test_data[] = {
    {1,' '},
    {0,'&'},
    {0,'1'},
    {1,'&',},
    {-1,0},
  };
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  char msg[256];
  int c;
  for(itest_data=test_data;itest_data->result!=-1;itest_data++) {
    sprintf(msg,"Identifier should%sbe found",not[itest_data->result]);
    TEST(msg,A_SEE_PARSER_IDENTIFIER(&myacp,"_a-zA-Z","_a-zA-Z0-9") == itest_data->result);
    TEST("Valid characters consumed",itest_data->next_chr == A_SEE_PARSER_PEEK_CHR(&myacp));
    for(;(c=A_SEE_PARSER_PEEK_CHR(&myacp)) && !isspace(c);A_SEE_PARSER_NEXT_CHR(&myacp));
    A_SEE_PARSER_WHITESPACE(&myacp," ");
  }
  A_SEE_PARSER_DESTROY(&myacp);

}

void test_quoted_string()
{
  TESTCASE("Quoted String");
  struct {
    const char* str;
    int result;
    int next_chr;
  } *itest_data,test_data[] = {
    { " \"This\" is a test" , 0, ' '},
    { "\"This is a test", 0, '"' },
    { "\"This\"A is a test", 1, 'A' },
    {"\"This\\\" is\"B a test" , 1, 'B'},
    { "\"This\\\" is\\\" a\"C test", 1, 'C' },
    { "\"This\\\" is\\\" a\\n test\"D", 1, 'D' },
    { "\"This\\\" is\\\" a\\n test", 0, '"'},
    { NULL, 0 },
  };
  char msg[256];
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->str;itest_data++) {
    SET_INPUT_BUFFER(itest_data->str)
    sprintf(msg,"Double quouted string should%sbe found",not[itest_data->result]);
    TEST(msg,A_SEE_PARSER_DOUBLE_QUOTED_STRING(&myacp) == itest_data->result);
    TEST("Valid characters consumed",itest_data->next_chr == A_SEE_PARSER_PEEK_CHR(&myacp));
    A_SEE_PARSER_RESET;
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_c_comment()
{
  TESTCASE("C Comment");
  input_buffer_pos=0;
  char msg[256];
  struct {
    const char* str;
    int result;
    int next_chr;
  } *itest_data,test_data[] =
  {
    { "/* this\nis\na\ncomment */A this is not", 1 , 'A'},
    { "B/* this does not match because first character is not start of comment*/", 0, 'B' },
    { "/* this\nis\na\ncomment because doesn't end properly", 0, '/' },
    { "/**/C", 1,'C'},
    { NULL, 0}
  };
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->str;itest_data++) {
    SET_INPUT_BUFFER(itest_data->str)
    sprintf(msg,"C comment should%sbe found",not[itest_data->result]);
    TEST(msg,A_SEE_PARSER_C_COMMENT(&myacp) == itest_data->result);
    TEST("Valid characters consumed",itest_data->next_chr == A_SEE_PARSER_PEEK_CHR(&myacp));
    A_SEE_PARSER_RESET;
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_nested_c_comment()
{
  TESTCASE("C Comment");
  input_buffer_pos=0;
  char msg[256];
  struct {
    const char* str;
    int result;
    int next_chr;
  } *itest_data,test_data[] =
  {
    { "/* this\nis\na\n/* nested */ comment*/A this is not", 1 , 'A'},
    { "B/* this does not match because /* first character is not */start of comment*/", 0, 'B' },
    { "/* this\nis\na\ncomment /*because doesn't end properly*/", 0, '/' },
    { "/*Deeper/*nested/*comment*/test*/ /*more nesting*/*/C",1,'C'},
    { "/*/**/*/C", 1,'C'},
    { NULL, 0}
  };
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->str;itest_data++) {
    SET_INPUT_BUFFER(itest_data->str)
    sprintf(msg,"Nested C comment should%sbe found",not[itest_data->result]);
    TEST(msg,A_SEE_PARSER_NESTED_C_COMMENT(&myacp) == itest_data->result);
    TEST("Valid characters consumed",itest_data->next_chr == A_SEE_PARSER_PEEK_CHR(&myacp));
    A_SEE_PARSER_RESET;
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_cpp_comment()
{
  TESTCASE("CPP Comment");
  test_data_t test_data[] =
  {
    {" // this is not a comment. It begins with a space rather than '//'" ,0,' '},
    {"//",1,0},
    {"// this is a comment \n this is not" ,1,'\n'},
    {"// this is a comment \r\n this is not" ,1,'\r'},
    { NULL, 0 },
  };
  test_data_t* itest_data;
  char msg[128];
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->str;itest_data++) {
    SET_INPUT_BUFFER(itest_data->str)
    sprintf(msg,"CPP comment should%sbe found",not[itest_data->result]);
    TEST(msg,A_SEE_PARSER_CPP_COMMENT(&myacp) == itest_data->result);
    printf("PEEK: %d\n",A_SEE_PARSER_PEEK_CHR(&myacp));
    TEST("Valid characters consumed",itest_data->next_chr == A_SEE_PARSER_PEEK_CHR(&myacp));
    A_SEE_PARSER_RESET;
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_basic_comment()
{
  TESTCASE("CPP Comment");
  test_data_t test_data[] =
  {
    {" rem this is not a comment. It begins with a space rather than '//'" ,0,' '},
    {"ReM",1,0},
    {"Remarkable that this is not a comment",0,'R'},
    {"rem this is a comment \n this is not" ,1,'\n'},
    {"rem this is a comment \r\n this is not" ,1,'\r'},
    { NULL, 0 },
  };
  test_data_t* itest_data;
  char msg[128];
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->str;itest_data++) {
    SET_INPUT_BUFFER(itest_data->str)
    sprintf(msg,"Basic comment should%sbe found",not[itest_data->result]);
    TEST(msg,A_SEE_PARSER_BASIC_COMMENT(&myacp) == itest_data->result);
    TEST("Valid characters consumed",itest_data->next_chr == A_SEE_PARSER_PEEK_CHR(&myacp));
    A_SEE_PARSER_RESET;
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_simple_sequence()
{
  TESTCASE("Simple sequence");
  input_buffer="abcdefghijklmnopqrstuvwxyz";
  input_buffer_pos=0;
  test_data_t test_data[] =
  {
    { "   ", 0, 'a'},
    { "aaa", 0, 'a'},
    { "abb", 0, 'a'},
    { "abc", 1, 'd'},
    { "abc", 0, 'd'},
    { NULL, -1, 'a'},
  };
  test_data_t* itest_data;
  char msg[128];
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->result != -1;itest_data++) {
    sprintf(msg,"Sequence is%ssuccessful",not[itest_data->result]);
    int rc = A_SEE_PARSER_SIMPLE_SEQUENCE(&myacp,
      A_SEE_PARSER_NEXT_CHR_IS(&myacp,*(itest_data->str)) &&
      A_SEE_PARSER_NEXT_CHR_IS(&myacp,*(itest_data->str+1)) &&
      A_SEE_PARSER_NEXT_CHR_IS(&myacp,*(itest_data->str+2))
    );
    TEST(msg,rc == itest_data->result);
    TEST("Proper characters consumed",A_SEE_PARSER_PEEK_CHR(&myacp) == itest_data->next_chr);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}


void test_sequence()
{
  TESTCASE("SEQUENCE");
  input_buffer="abcdefghijklmnopqrstuvwxyz";
  input_buffer_pos=0;
  test_data_t test_data[] =
  {
    { "   ", 0, 'a'},
    { "aaa", 0, 'a'},
    { "abb", 0, 'a'},
    { "abc", 1, 'd'},
    { "abc", 0, 'd'},
    { NULL, -1, 'a'},
  };
  test_data_t* itest_data;
  char msg[128];
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->result != -1;itest_data++) {
    int success_executed=0;
    int failure_executed=0;
    sprintf(msg,"Sequence is%ssuccessful",not[itest_data->result]);
    int rc = A_SEE_PARSER_SEQUENCE(&myacp,
      A_SEE_PARSER_NEXT_CHR_IS(&myacp,*(itest_data->str)) &&
      A_SEE_PARSER_NEXT_CHR_IS(&myacp,*(itest_data->str+1)) &&
      A_SEE_PARSER_NEXT_CHR_IS(&myacp,*(itest_data->str+2)),
      success_executed=1;,
      failure_executed=1;
    );
    TEST(msg,rc == itest_data->result);
    sprintf(msg,"Success action%sexecuted",not[itest_data->result]);
    TEST(msg,success_executed==itest_data->result);
    sprintf(msg,"Failure action%sexecuted",not[FLIP(itest_data->result)]);
    TEST(msg,failure_executed==FLIP(itest_data->result));
    TEST("Proper characters consumed",A_SEE_PARSER_PEEK_CHR(&myacp) == itest_data->next_chr);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_zero_or_more()
{
  TESTCASE("ZERO OR MORE");
  input_buffer="abcabcabcabc";
  input_buffer_pos=0;
  test_data_t test_data[] = {
    {"def",0,'a'},
    {"abcd",0,'a'},
    {"abc",4,0},
    {"abc",0,0},
    {NULL,-1,0}
  };
  test_data_t* itest_data;
  char msg[128];
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->result != -1;itest_data++) {
    int success_count=0;
    int failure_count=0;
    sprintf(msg,"Sequence successful %d times",itest_data->result);
    int rc = A_SEE_PARSER_ZERO_OR_MORE(&myacp,
      A_SEE_PARSER_LITERAL(&myacp,itest_data->str),
      success_count++;,
      failure_count++;
    );
    TEST("ZERO OR MORE result is successful",rc);
    sprintf(msg,"Success executed %d times",itest_data->result);
    TEST(msg,itest_data->result == success_count);
    TEST("Failure executed 1 time",failure_count==1);
    TEST("Proper characters consumed",A_SEE_PARSER_PEEK_CHR(&myacp) == itest_data->next_chr);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_one_or_more()
{
  TESTCASE("ONE OR MORE");
  input_buffer="abcabcabcabc";
  input_buffer_pos=0;
  test_data_t test_data[] = {
    {"def",0,'a'},
    {"abcd",0,'a'},
    {"abc",4,0},
    {"abc",0,0},
    {NULL,-1,0}
  };
  test_data_t* itest_data;
  char msg[128];
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->result != -1;itest_data++) {
    int success_count=0;
    int failure_count=0;
    sprintf(msg,"Sequence successful %d times",itest_data->result);
    int rc = A_SEE_PARSER_ZERO_OR_MORE(&myacp,
      A_SEE_PARSER_LITERAL(&myacp,itest_data->str),
      success_count++;,
      failure_count++;
    );
    sprintf(msg,"ONE OR MORE result is%ssuccessful",not[itest_data->result>0]);
    TEST(msg,rc);
    sprintf(msg,"Success executed %d times",itest_data->result);
    TEST(msg,itest_data->result == success_count);
    TEST("Failure executed 1 time",failure_count==1);
    TEST("Proper characters consumed",A_SEE_PARSER_PEEK_CHR(&myacp) == itest_data->next_chr);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_optional()
{
  TESTCASE("Optional");
  input_buffer="abcefg"; // <-- note there isn't a "d"
  input_buffer_pos=0;
  test_data_t test_data[] = {
    {"abcd",0,'a'},
    {"abc",1,'e'},
    {"d",0,'e'},
    {"efg",1,0},
    {"efg",0,0},
    {NULL,-1,0}
  };
  test_data_t* itest_data;
  char msg[128];
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->result != -1;itest_data++) {
    int success_count=0;
    int failure_count=0;
    sprintf(msg,"Sequence successful %d times",itest_data->result);
    int rc = A_SEE_PARSER_OPTIONAL(&myacp,
      A_SEE_PARSER_LITERAL(&myacp,itest_data->str),
      success_count++;,
      failure_count++;
    );
    sprintf(msg,"ONE OR MORE result is%ssuccessful",not[itest_data->result>0]);
    TEST("Optional result is successful",rc);
    sprintf(msg,"Success executed %d times",itest_data->result);
    TEST(msg,itest_data->result == success_count);
    int fail_count_should_be = 1 - itest_data->result;
    sprintf(msg,"Failure executed %d times",fail_count_should_be);
    TEST(msg,failure_count==fail_count_should_be);
    TEST("Proper characters consumed",A_SEE_PARSER_PEEK_CHR(&myacp) == itest_data->next_chr);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_non_consuming()
{
  TESTCASE("Non consuming");
  input_buffer="abcdefghijklmnopqrstuvwxyz";
  input_buffer_pos=0;
  test_data_t test_data[] = {
    {"def",0,'a'},
    {"abcabc",0,'a'},
    {"abc",1,'a'},
    {"abcdefghijklmnopqrstuvwxyz",1,'a'},
    {NULL,-1,'a'}
  };
  test_data_t* itest_data;
  char msg[128];
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->result != -1;itest_data++) {
    sprintf(msg,"Non consuming rule is%ssuccessful",not[itest_data->result]);
    int rc = A_SEE_PARSER_NON_CONSUMING(&myacp,A_SEE_PARSER_LITERAL(&myacp,itest_data->str));
    TEST(msg,rc == itest_data->result);
    TEST("No characters consumed",A_SEE_PARSER_PEEK_CHR(&myacp) == itest_data->next_chr);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_not()
{
  TESTCASE("Non consuming");
  input_buffer="abcdefghijklmnopqrstuvwxyz";
  input_buffer_pos=0;
  test_data_t test_data[] = {
    {"def",1,'a'},
    {"abcabc",1,'a'},
    {"abc",0,'a'},
    {"abcdefghijklmnopqrstuvwxyz",0,'a'},
    {NULL,-1,'a'}
  };
  test_data_t* itest_data;
  char msg[128];
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->result != -1;itest_data++) {
    sprintf(msg,"Not rule is%ssuccessful",not[itest_data->result]);
    int rc = A_SEE_PARSER_NOT(&myacp,A_SEE_PARSER_LITERAL(&myacp,itest_data->str));
    TEST(msg,rc == itest_data->result);
    TEST("No characters consumed",A_SEE_PARSER_PEEK_CHR(&myacp) == itest_data->next_chr);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

#define WORD A_SEE_PARSER_IDENTIFIER(&myacp,"_a-zA-Z","_a-zA-Z0-9")
void test_yytext()
{
  TESTCASE("YYTEXT");
  input_buffer="This is a test";
  input_buffer_pos=0;
  test_data_t test_data[] =
  {
    { "This" , 1 , 'i'},
    { "is", 1, 'a'},
    { "a", 1,'t'},
    { "test",1 ,0},
    { NULL , -1 , 0},
  };
  test_data_t* itest_data;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->str;itest_data++) {
    int rc = A_SEE_PARSER_CAPTURE_BEGIN(&myacp) && WORD && A_SEE_PARSER_CAPTURE_END(&myacp) && (A_SEE_PARSER_WHITESPACE(&myacp," \t"),1);
    TEST("Sequence matched",rc);
    TEST("Correct text captured",strcmp(A_SEE_PARSER_YYTEXT(&myacp),itest_data->str)==0);
    A_SEE_PARSER_FLUSH(&myacp);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

void test_yytext_again()
{
  TESTCASE("YYTEXT AGAIN");
  input_buffer="This is a test";
  input_buffer_pos=0;
  test_data_t test_data[] =
  {
    { "This" , 1 , 'i'},
    { "is", 1, 'a'},
    { "a", 1,'t'},
    { "test",1 ,0},
    { NULL , -1 , 0},
  };
  test_data_t* itest_data;
  a_see_parser_t myacp; A_SEE_PARSER_INIT(&myacp);
  for(itest_data=test_data;itest_data->str;itest_data++) {
    int rc = A_SEE_PARSER_CAPTURE(&myacp,WORD,) && (A_SEE_PARSER_WHITESPACE(&myacp," \t"),1);
    TEST("Sequence matched",rc);
    TEST("Correct text captured",strcmp(A_SEE_PARSER_YYTEXT(&myacp),itest_data->str)==0);
    A_SEE_PARSER_FLUSH(&myacp);
  }
  A_SEE_PARSER_DESTROY(&myacp);
}

test_function tests[] =
{
  test_init_destroy,
  test_buffer,
  test_peek_next_chr,
  test_next_chr_is,
  test_next_chr_is_not,
  test_char_set,
  test_not_char_set,
  test_whitespace,
  test_eol,
  test_literal,
  test_binary_integer,
  test_octal_integer,
  test_decimal_integer,
  test_hex_integer,
  test_floating_point,
  test_identifier,
  test_quoted_string,
  test_c_comment,
  test_nested_c_comment,
  test_cpp_comment,
  test_basic_comment,
  test_simple_sequence,
  test_sequence,
  test_zero_or_more,
  test_one_or_more,
  test_optional,
  test_non_consuming,
  test_not,
  test_yytext,
};

TEST_MAIN(tests)
