# Tests for a-see_parser.

The four files contain the same test.
* test_a_see_parser.c -- run test using long form (i.e. A_SEE_PARSER_SEQUENCE)
* test_a_see_parser_short.c -- run test using short form (i.e. SEQUENCE)
* test_a_see_parser_local.c -- run test using long form for multi instance (i.e. A_SEE_PARSER_SEQUENCE(acp, ..))
* test_a_see_parser_local_short.c -- run test using short form for multi instnace (i.e. SEQUENCE(acp,...))
