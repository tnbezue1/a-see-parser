# Json parser

Parses a json file read from standard input

## Syntax
    ./json_parser < input.json
    cat input.json | ./json_parser

