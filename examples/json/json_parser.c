
/*
  - json_value? EOL

  json_value = json_object | json_array | json_string | json_real | json_integer | json_bool | json_null

  json_object = '{' (json_pair (, json_pair)* [,]?)? '}'

  json_pair = json_string ':' json_value

  json_array = '[' (json_value ( , json_value)* [,]?)? ']'

  json_string = '"' chars '"'

  json_real = real_number

  json_integer = integer

  json_bool = true | false

  json_null = "null"

*/


//#define A_SEE_PARSER_TRACE
#define A_SEE_PARSER_USE_BUILTIN_TERMINALS
#define A_SEE_PARSER_USE_SHORT_FORM
#include <a_see_parser/a_see_parser.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#define SPACING (WHITESPACE(" \t\r\n"),1)
#define ALPHA CHAR_SET("_a-zA-Z")
#define ALNUM CHAR_SET("_a-zA-Z0-9")
#define JSON_NULL SIMPLE_SEQUENCE(LITERAL("null") && !ALNUM && SPACING)
#define JSON_TRUE SIMPLE_SEQUENCE(LITERAL("true") && !ALNUM && SPACING)
#define JSON_FALSE SIMPLE_SEQUENCE(LITERAL("false") && !ALNUM && SPACING)
#define LBRACE NEXT_CHR_IS('{') && SPACING
#define RBRACE NEXT_CHR_IS('}') && SPACING
#define LBRACKET NEXT_CHR_IS('[') && SPACING
#define RBRACKET NEXT_CHR_IS(']') && SPACING
#define COMMA NEXT_CHR_IS(',') && SPACING
#define COLON NEXT_CHR_IS(':') && SPACING
#define BARE_STRING SIMPLE_SEQUENCE(!JSON_NULL && !JSON_TRUE && !JSON_FALSE && IDENTIFIER("_a-zA-Z","_a-zA-Z0-9"))

int json_value();
int json_object();
int json_pair();
int json_array();
int json_string();
int json_real();
int json_integer();
int json_bool();
int json_null();

int json_null()
{
  return JSON_NULL;
}

int json_bool()
{
  return JSON_TRUE || JSON_FALSE;
}

// If the value is to be captured, use SEQUENCE and CAPTURE_ON/CAPTURE_OFF
int json_integer()
{
  return SIMPLE_SEQUENCE(DECIMAL_INTEGER && !ALNUM) && SPACING;
}

int json_real()
{
  return SIMPLE_SEQUENCE(FLOATING_POINT && !ALNUM) && SPACING;
}

int json_string()
{
  return (DOUBLE_QUOTED_STRING || BARE_STRING) && SPACING;
}

int json_array()
{
  return SIMPLE_SEQUENCE(LBRACKET && ZERO_OR_MORE(json_value() && COMMA,,)
      && OPTIONAL(json_value(),,) && RBRACKET);
/*
    Also a possible implementation
    ({
      int rc = LBRACKET;
      if(rc) {
        while(json_value() && COMMA);
        rc=RBRACKET;
      }
      rc;
    })
  );*/
}

int json_pair()
{
  return SIMPLE_SEQUENCE(json_string() && COLON && json_value());
}

int json_object()
{
  return SIMPLE_SEQUENCE(LBRACE && ZERO_OR_MORE(json_pair() && COMMA,,)
      && OPTIONAL(json_pair(),,) && RBRACE);
/*
  Also a possible implementation
  return ({
      int rc = LBRACE;
      if(rc) {
        while(json_pair() && COMMA);
        rc=RBRACE;
      }
      rc;
    })
  );*/
}

int json_value()
{
  return
      json_object()
      ||
      json_array()
      ||
      json_string()
      ||
      json_real()
      ||
      json_integer()
      ||
      json_bool()
      ||
      json_null();
}

int parse_json()
{
  return SPACING && OPTIONAL(json_value(),,) && !ANY;
}

int main(int argc,char* argv[])
{
  int rc;
  A_SEE_PARSER_INIT;
  if((rc=parse_json()))
    printf("successful.\n");
  else
    printf("failed\n");
  A_SEE_PARSER_DESTROY;
  return 0;
}
