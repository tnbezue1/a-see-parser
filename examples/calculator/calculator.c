
/*
		Copyright (C) 2021-2022  by Terry N Bezue

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
  Multithreaded calculator to demonstrate multiple instances of parser
  Uses muliple threads to perform caaculations contained in given files
  Input file names should end with ".inp" (or any 3 letters).
  Output will be in file with extension ".out".
  Usage:
    ./caculator -n num_threads file1.inp [ [ file2.inp ] ... filen.inp ]
  Output will be in file1.out, file2.out, ..., filen.out

  See test1.inp for explanation of input file format
*/

/*
  PEG grammar for calculator

  expr <- term

  term <- factor ( ( '+' / '-' ) factor )*

  factor <- ( ('+' / '-' ) factor ) / ( power ( ( '*' / '/' ) power)* )

  power <- value ( '^' power )*

  value <- number / '(' term ')' / function / constant / variable

  function <- ident '(' term (, term)? ')'

  constant <- 'pi' / 'ne'

  variable <- [a-zA-Z]

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

/* Before include a_see_parser.h, several optinal defines can be made */
/* The Input Method. If A_SEE_PARSER_INPUT is not defined, a default one will be used */
#define A_SEE_PARSER_INPUT(ACP_PTR,buf,result,max_size) \
{ \
  int c = fgetc((ACP_PTR)->input); \
  result = (c == EOF) ? 0 : (*(buf) = c,1); \
}

/* To trace rule execution, define A_SEE_PARSER_TRACE.  A large amount of output is printed to STDOUT */
//#define A_SEE_PARSER_TRACE

// Define A_SEE_PARSER_LOCAL for multiple parsers and/or if extra structure members are to be added
#define A_SEE_PARSER_LOCAL
// Extra entries for custom a see parser
#define A_SEE_PARSER_MEMBERS \
  FILE* input; \
  double *vars; // the input file handle and pointer to variables (a-z).
// Use builtin terminals
#define A_SEE_PARSER_USE_BUILTIN_TERMINALS
// Use short form
#define A_SEE_PARSER_USE_SHORT_FORM

/* Now the header can be included */
#include <a_see_parser/a_see_parser.h>

/* Terminals */
#define SPACING(ACP) (WHITESPACE(ACP," \t"),SHELL_COMMENT(ACP),1)
#define ASSIGN(ACP) NEXT_CHR_IS(ACP,'=') && SPACING(ACP)
#define PLUS(ACP) NEXT_CHR_IS(ACP,'+') && SPACING(ACP)
#define MINUS(ACP) NEXT_CHR_IS(ACP,'-') && SPACING(ACP)
#define TIMES(ACP) NEXT_CHR_IS(ACP,'*') && SPACING(ACP)
#define DIVIDE(ACP) NEXT_CHR_IS(ACP,'/') && SPACING(ACP)
#define POWER(ACP) NEXT_CHR_IS(ACP,'^') && SPACING(ACP)
#define LPAREN(ACP) NEXT_CHR_IS(ACP,'(') && SPACING(ACP)
#define RPAREN(ACP) NEXT_CHR_IS(ACP,')') && SPACING(ACP)
#define COMMA(ACP) NEXT_CHR_IS(ACP,',') && SPACING(ACP)
#define ALNUM(ACP) CHAR_SET(ACP,"_a-zA-Z0-9")
#define INTEGER(ACP) ONE_OR_MORE(ACP,CHAR_SET(ACP,"0-9"),,)

/* Math functions */
typedef double (*one_arg_math_function)(double);
typedef double (*two_arg_math_function)(double,double);
typedef struct {
  const char* name;   // function name
  one_arg_math_function func1;  // method for one arg function or NULL if two arg function
  two_arg_math_function func2;  // method for two arg function or NULL if one arg function
} math_function_t;

static math_function_t functions[] = {
  { "abs", fabs ,NULL },
  { "acos", acos ,NULL },
  { "asin", asin,NULL },
  { "atan", atan,NULL },
  { "atan2" , NULL, atan2 },
  { "ceil", ceil,NULL },
  { "cos", cos,NULL },
  { "cosh", cosh,NULL },
  { "exp", exp,NULL },
  { "fabs", fabs,NULL },
  { "floor", floor,NULL },
  { "fmod",NULL,fmod },
  { "ln", log,NULL },
  { "log", log10,NULL },
  { "log10", log10,NULL },
  { "loge", log,NULL },
  { "pow" , NULL, pow},  // Maybe don't need this because of '^' operator
  { "sin", sin,NULL },
  { "sinh", sinh,NULL },
  { "sqrt", sqrt,NULL },
  { "tan", tan,NULL },
  { "tanh", tanh,NULL },
  { NULL, NULL }
};

/*
  returns address of function. NULL if not found
*/
const math_function_t* get_math_function(const char* name)
{
  const math_function_t* i = functions;
  for(;i->name != NULL;i++)
    if(strcmp(name,i->name) == 0)
      return i;
  return NULL;
}

/*
  Constants
*/
typedef struct {
  const char* name;
  double value;
} constant_t;

constant_t constants[] = {
  { "pi", M_PI},
  { "ne", M_E}, // natural e -- just 'e' would conflict with variable 'e'
  { NULL,0}
};

const constant_t* get_constant(const char* name)
{
  const constant_t* i = constants;
  for(;i->name != NULL;i++)
    if(strcmp(name,i->name) == 0)
      return i;
  return NULL;
}


void* calculator_parser(void*);
int expr(a_see_parser_t*,double*);
int term(a_see_parser_t*,double*);
int factor(a_see_parser_t*,double*);
int power(a_see_parser_t*,double*);
int value(a_see_parser_t*,double*);
int function(a_see_parser_t*,double*);
int number(a_see_parser_t*,double*);
int constant(a_see_parser_t*,double*);
int variable(a_see_parser_t*,int*);


/*
  Simple thread data definition.
*/
typedef struct {
  pthread_t tid_;  // thread id
  const char* fname_; // Input file to process
  int state_; // 0 - available , 1 - running, 2 - finished
} thread_data_t;

/* Thread entry point */
void* calculator_parser(void* data_ptr)
{
  a_see_parser_t acp_calc;
  thread_data_t* td = (thread_data_t*)data_ptr;
  acp_calc.input = fopen(td->fname_,"r");
  if(acp_calc.input) {
    // For this demo, the input file name ends with ".inp" -- actually any 3 characters (i.e. samp.imp)
    // output file will be samp.out
    unsigned int len = strlen(td->fname_);
    char* out_fname=malloc(len+1);
    strncpy(out_fname,td->fname_,len-3);
    strcpy(out_fname+len-3,"out");
    FILE*output=fopen(out_fname,"w");
    if(output) {
      A_SEE_PARSER_INIT(&acp_calc);
      acp_calc.vars = (double*)malloc(26*sizeof(double));
      memset(acp_calc.vars,0,26*sizeof(double));
      double val;
      int rc=1;
      while(rc) {
        rc =
          SEQUENCE(&acp_calc,SPACING(&acp_calc) && EOL(&acp_calc),fprintf(output,"\n");, ) // ignore blank or comment lines.
          ||
          SEQUENCE(&acp_calc,expr(&acp_calc,&val),
          // Successful expression
          fprintf(output,"%lg\n",val);
          ,
            // Failed to match expression
            // Coulud used this to rather than error below
            // while(!EOL);
          )
          ||
          SEQUENCE(&acp_calc,ZERO_OR_MORE(&acp_calc,!EOL(&acp_calc) && ANY(&acp_calc),,)
            && EOL(&acp_calc),fprintf(output,"Error\n");,);
        FLUSH(&acp_calc); // discard already consumed input
      }
      free(acp_calc.vars);
      fclose(output);
      A_SEE_PARSER_DESTROY(&acp_calc);
    }
    free(out_fname);
    fclose(acp_calc.input);
  } else {
    fprintf(stderr,"Failed to open %s\n",td->fname_);
  }
  td->state_=2;
  pthread_exit(NULL);
}

// expr <- term !.
int expr(a_see_parser_t* acp,double* val)
{
  int index=-1;
  return SEQUENCE(acp,SPACING(acp) && OPTIONAL(acp,variable(acp,&index) && ASSIGN(acp),,) &&
      term(acp,val) && EOL(acp),if(index>=0) acp->vars[index]=*val;,);
}

/*
  NOTE: A long sequence can be difficult for debuggers (gdb, nemiver, gede, etc).  When
  it is excuted, it's difficult to determine exactly which part of the sequence is in error.
  For debugging, it may be better to break the sequence into parts. For example, the
  "expr" method above may be written as follows.

    int expr(a_see_parser_t* acp,double* val)
    {
      int index=-1;
      int rc = SEQUENCE(acp,SPACING(acp),,);
      // For this parser, SPACING always returns true, but use "if" as a demonstration.
      if(rc) {
        OPTIONAL(acp,variable(acp,&index) && ASSIGN(acp),,); // optional is always true
        rc = term(acp,val) && EOL(acp);
        if (rc) {
          if(index>=0) acp->vars[index]=*val;
        }
      }
      return rc;
    }

  Debug version of other methods is left as an exercise for the user.
*/

//  term <- factor ( ( '+' { c = '+'; } ? '-' { c='-'; } ) factor )*
int term(a_see_parser_t* acp,double* lval)
{
  int c;
  double rval;
  return factor(acp,lval) &&
    ZERO_OR_MORE(acp,((PLUS(acp) && (c='+')) || (MINUS(acp) && (c='-'))) && factor(acp,&rval),
      if(c == '+')
        *lval += rval;
      else
        *lval -= rval;
    ,);
}

//  factor = ( ('+' | '-' ) factor ) | ( power ( ( '*' | '/' ) power)* )
int factor(a_see_parser_t* acp,double* lval)
{
  int c;
  double rval;
  return SEQUENCE(acp,((PLUS(acp) && (c='+')) || (MINUS(acp) && (c='-'))) && factor(acp,lval),
    if(c == '-')
      *lval = -*lval;
    ,)
  ||
  (power(acp,lval) &&
        ZERO_OR_MORE(acp,((TIMES(acp) && (c='*')) || (DIVIDE(acp) && (c='/'))) && power(acp,&rval),
          if(c=='*')
            *lval *= rval;
          else
            *lval /= rval;
        ,));
}

//  power = value ( '^' power )*
int power(a_see_parser_t* acp,double* lval)
{
  double rval;
  return value(acp,lval) && ZERO_OR_MORE(acp,POWER(acp) && power(acp,&rval),
    *lval = pow(*lval,rval);
  ,);
}

//  value = number | '(' term ')' | function | variable
int value(a_see_parser_t* acp,double* val)
{
  int index;
  return number(acp,val)
    ||
    SEQUENCE(acp,LPAREN(acp) && term(acp,val) && RPAREN(acp),,)
    ||
    constant(acp,val)
    ||
    function(acp,val)
    ||
    (variable(acp,&index) && (*val = acp->vars[index],1));
}

// number <- REAL / INTEGER
int number(a_see_parser_t* acp,double* val)
{
  return SEQUENCE(acp,CAPTURE_BEGIN(acp) && (FLOATING_POINT(acp) || INTEGER(acp)) && CAPTURE_END(acp) && !ALNUM(acp) && SPACING(acp),
    *val=atof(YYTEXT(acp));
  ,);
}

// constant <- pi || e
//  function = ident '(' term ')'
int function(a_see_parser_t* acp,double* val)
{
  char* func_name = NULL;
  int has_two_args=0;
  double val1,val2;
  return SEQUENCE(acp,CAPTURE_BEGIN(acp) && IDENTIFIER(acp,"a-zA-Z","a-zA-Z0-9") && CAPTURE_END(acp) && SPACING(acp) &&
    (func_name = strdup(YYTEXT(acp))) &&
    LPAREN(acp) && term(acp,&val1) && OPTIONAL(acp,COMMA(acp) && term(acp,&val2),has_two_args=1;,) && RPAREN(acp) &&
      ({
        int rc=0;
        const math_function_t* func= get_math_function(func_name);
        if(func) {
          if(has_two_args) {
            if(func->func2) {
              *val = func->func2(val1,val2);
              rc =1;
            } else
              fprintf(stderr,"%s requires one argument.\n",func_name);
          } else {
            if(func->func1) {
              *val = func->func1(val1);
              rc =1;
            } else
              fprintf(stderr,"%s requires two arguments.\n",func_name);
          }
        } else {
          fprintf(stderr,"Unknown function: %s\n",func_name);
        }
        rc;
      }) && SPACING(acp)
    ,
      free(func_name);
    ,
      if(func_name)
        free(func_name);
  );
}

int constant(a_see_parser_t* acp,double* val)
{
  int rc=0;
  return SEQUENCE(acp,CAPTURE_BEGIN(acp) && IDENTIFIER(acp,"a-zA-Z","a-zA-Z0-9") && SPACING(acp)
    && !ALNUM(acp) && CAPTURE_END(acp)
    && ({
      const constant_t* cnst = get_constant(YYTEXT(acp));
      if(cnst) {
        *val = cnst->value;
        rc=1;
      }
      rc;
      }),
  ,);
}

int variable(a_see_parser_t* acp,int* index)
{
  return SEQUENCE(acp,CAPTURE_BEGIN(acp) && CHAR_SET(acp,"a-zA-Z") && !ALNUM(acp) && CAPTURE_END(acp) && SPACING(acp),
    *index = tolower(*YYTEXT(acp))-'a';
  ,);
}

unsigned int n_threads = 5;
thread_data_t* thread_pool;

void create_thread_pool()
{
  int i;
  thread_pool = (thread_data_t*)malloc(n_threads*sizeof(thread_data_t));
  for(i=0;i<n_threads;i++)
    thread_pool[i].state_=0;
}

thread_data_t* next_available_thread()
{
  static unsigned int ithread=0;
  while(1) {
    if(ithread == n_threads)
      ithread=0;
    for(;ithread<n_threads;ithread++) {
      if(thread_pool[ithread].state_ == 2) {
        pthread_join(thread_pool[ithread].tid_,NULL);
        thread_pool[ithread].state_ = 0;
        return thread_pool+ithread++;
      }
      if(thread_pool[ithread].state_ == 0)
        return thread_pool+ithread++;
    }
  }
}

void destroy_thread_pool()
{
  // join remaining threads
  int i=0;
  int n_remaining;
  do {
    n_remaining=0;
    for(i=0;i<n_threads;i++) {
      if(thread_pool[i].state_ == 2) {
        pthread_join(thread_pool[i].tid_,NULL);
        thread_pool[i].state_=0;
      }
      if(thread_pool[i].state_ == 1)
        n_remaining++;
    }
  } while(n_remaining > 0);
  free(thread_pool);
}

long diff_in_nanoseconds(struct timespec start, struct timespec end)
{
    struct timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp.tv_sec*1000000000 + temp.tv_nsec;
}

int main(int argc,char* argv[])
{
  if(argc > 1) {
    int i=1;
    if(strcmp(argv[1],"-n")==0) {
      n_threads=atoi(argv[2]);
      if(n_threads == 0)
        n_threads=5;
      else if(n_threads > 10)
        n_threads=10;
      i=3;
    }
    int ninp = argc - i;
    printf("Using %u threads\n",n_threads);
    struct timespec start,end;
    create_thread_pool();
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
    thread_data_t *td;
    for(;i<argc;i++) {
      td = next_available_thread();
      td->state_=1;
      td->fname_ = argv[i];
      pthread_create(&td->tid_,NULL,calculator_parser,td);
    }
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
    destroy_thread_pool();
    printf("Time to process %d input files: %lg seconds\n",ninp,((double)diff_in_nanoseconds(start,end))/1000000000.0);
  }
}
