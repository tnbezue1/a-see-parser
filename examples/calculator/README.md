# Calculator

## Multiple a see parsers

The caluclator example demonstrates using multiple a-see-parser instances.
calculator uses multiple threads to perform calculations specfied in input files.
Output is the name of the input file with "inp" replaced with "out".

### Syntax
    ./calculator [ -n N ] file1.inp [ file2.inp ... filen.inp ]

    N - number of threads to use (1-10). The default is 5
    file?.inp -- input file

calculator reports the elapsed time to perform calculations.
Varing the number of threads may be interesting.
However, a large number of input files is required to get any meaningful differences.

### Examples

    ./calculator test1.inp -- perform calculations in test1.inp. Output in test1.out
    ./calculator *.inp -- perform calculations in all input files using 5 threads. Output in *.out
    ./calculator -n 3 *.inp -- perform calculations in all input files using 3 threads. Output in *.out
    ./calculator -n 3 *.inp *.inp *.inp *.inp -- perform calculations in all input files using 3 threads repeated 4 times. Output in *.out


## Tests

* test1.inp -- Simple iteration
* test2.inp -- y = x^2 from -10 to 10
* test3.inp -- Estimating pi
* test4.inp -- Another way to estimate pi
* test5.inp -- Illustrate derivative of y=x^2-3x+2 at 3.0 with decreasing delta
* test6.inp -- Area of rectangle with various lengths and widths
* test7.inp -- Area of circle with different radius
* test8.inp -- X and Y coordinates of vector
* test9.inp -- 4 simple expressions repeated 3000 times
* test10.inp -- estimate natural e
* test11.inp -- estimate square roots
