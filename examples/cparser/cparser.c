
/*
		Copyright (C) 2021  by Terry N Bezue

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
  The PEG grammar presented here is from
  https://github.com/pointlander/peg/blob/master/grammars/c/c.peg
*/

#include <stdio.h>
#include <stdlib.h>
#define A_SEE_PARSER_TRACE
#define A_SEE_PARSER_USE_BUILTIN_TERMINALS
#define A_SEE_PARSER_USE_SHORT_FORM

#include <a_see_parser/a_see_parser.h>

#define ID_CHAR CHAR_SET("_a-zA-Z0-9")
#define ID_NONDIGIT CHAR_SET("_a-zA-Z")

#define SPACING (WHITESPACE(" \t\r\n"),1)
#define AUTO (LITERAL("auto") && !ID_CHAR && SPACING)
#define BREAK (LITERAL("break") && !ID_CHAR && SPACING)
#define CASE (LITERAL("case") && !ID_CHAR && SPACING)
#define CHAR (LITERAL("char") && !ID_CHAR && SPACING)
#define CONST (LITERAL("const") && !ID_CHAR && SPACING)
#define CONTINUE (LITERAL("continue") && !ID_CHAR && SPACING)
#define DEFAULT (LITERAL("default") && !ID_CHAR && SPACING)
#define DOUBLE (LITERAL("double") && !ID_CHAR && SPACING)
#define DO (LITERAL("do") && !ID_CHAR && SPACING)
#define ELSE (LITERAL("else") && !ID_CHAR && SPACING)
#define ENUM (LITERAL("enum") && !ID_CHAR && SPACING)
#define EXTERN (LITERAL("extern") && !ID_CHAR && SPACING)
#define FLOAT (LITERAL("float") && !ID_CHAR && SPACING)
#define FOR (LITERAL("for") && !ID_CHAR && SPACING)
#define GOTO (LITERAL("goto") && !ID_CHAR && SPACING)
#define IF (LITERAL("if") && !ID_CHAR && SPACING)
#define INT (LITERAL("int") && !ID_CHAR && SPACING)
#define INLINE (LITERAL("inline") && !ID_CHAR && SPACING)
#define LONG (LITERAL("long") && !ID_CHAR && SPACING)
#define REGISTER (LITERAL("register") && !ID_CHAR && SPACING)
#define RESTRICT (LITERAL("restrict") && !ID_CHAR && SPACING)
#define RETURN (LITERAL("return") && !ID_CHAR && SPACING)
#define SHORT (LITERAL("short") && !ID_CHAR && SPACING)
#define SIGNED (LITERAL("signed") && !ID_CHAR && SPACING)
#define SIZEOF (LITERAL("sizeof") && !ID_CHAR && SPACING)
#define STATIC (LITERAL("static") && !ID_CHAR && SPACING)
#define STRUCT (LITERAL("struct") && !ID_CHAR && SPACING)
#define SWITCH (LITERAL("switch") && !ID_CHAR && SPACING)
#define TYPEDEF (LITERAL("typedef") && !ID_CHAR && SPACING)
#define UNION (LITERAL("union") && !ID_CHAR && SPACING)
#define UNSIGNED (LITERAL("unsigned") && !ID_CHAR && SPACING)
#define VOID (LITERAL("void") && !ID_CHAR && SPACING)
#define VOLATILE (LITERAL("volatile") && !ID_CHAR && SPACING)
#define WHILE (LITERAL("while") && !ID_CHAR && SPACING)
#define BOOL (LITERAL("_Bool") && !ID_CHAR && SPACING)
#define COMPLEX (LITERAL("_Complex") && !ID_CHAR && SPACING)
#define IMAGINARY (LITERAL("_Imaginary") && !ID_CHAR && SPACING)
#define STDCALL (LITERAL("_stdcall") && !ID_CHAR && SPACING)
#define DECLSPEC (LITERAL("__declspec") && !ID_CHAR && SPACING)
#define ATTRIBUTE (LITERAL("__attribute__") && !ID_CHAR && SPACING)

#define ELLIPSIS (LITERAL("...") && SPACING)
#define LEFTEQU (LITERAL("<<=") && SPACING)
#define RIGHTEQU (LITERAL(">>=") && SPACING)
#define PTR (LITERAL("->") && SPACING)
#define INC (LITERAL("++") && SPACING)
#define DEC (LITERAL("--") && SPACING)
#define LEFT (LITERAL("<<") && SPACING)
#define RIGHT (LITERAL(">>") && SPACING)
#define LE (LITERAL("<=") && SPACING)
#define GE (LITERAL(">=") && SPACING)
#define EQUEQU (LITERAL("==") && SPACING)
#define BANGEQU (LITERAL("!=") && SPACING)
#define ANDAND (LITERAL("&&") && SPACING)
#define OROR (LITERAL("||") && SPACING)
#define STAREQU (LITERAL("*=") && SPACING)
#define DIVEQU (LITERAL("/=") && SPACING)
#define MODEQU (LITERAL("%=") && SPACING)
#define PLUSEQU (LITERAL("+=") && SPACING)
#define MINUSEQU (LITERAL("-=") && SPACING)
#define ANDEQU (LITERAL("&=") && SPACING)
#define HATEQU (LITERAL("^=") && SPACING)
#define OREQU (LITERAL("|=") && SPACING)
#define LBRK (NEXT_CHR_IS('[') && SPACING)
#define RBRK (NEXT_CHR_IS(']') && SPACING)
#define LPAR (NEXT_CHR_IS('(') && SPACING)
#define RPAR (NEXT_CHR_IS(')') && SPACING)
#define LWING (NEXT_CHR_IS('{') && SPACING)
#define RWING (NEXT_CHR_IS('}') && SPACING)
#define DOT (NEXT_CHR_IS('.') && SPACING)
#define AND (NEXT_CHR_IS('&') && SPACING)
#define STAR (NEXT_CHR_IS('*') && SPACING)
#define PLUS (NEXT_CHR_IS('+') && SPACING)
#define MINUS (NEXT_CHR_IS('-') && SPACING)
#define TILDA (NEXT_CHR_IS('~') && SPACING)
#define BANG (NEXT_CHR_IS('!') && SPACING)
#define DIV (NEXT_CHR_IS('/') && SPACING)
#define MOD (NEXT_CHR_IS('%') && SPACING)
#define LT (NEXT_CHR_IS('<') && SPACING)
#define GT (NEXT_CHR_IS('>') && SPACING)
#define HAT (NEXT_CHR_IS('^') && SPACING)
#define OR (NEXT_CHR_IS('|') && SPACING)
#define QUERY (NEXT_CHR_IS('?') && SPACING)
#define COLON (NEXT_CHR_IS(':') && SPACING)
#define SEMI (NEXT_CHR_IS(';') && SPACING)
#define EQU (NEXT_CHR_IS('=') && SPACING)
#define COMMA (NEXT_CHR_IS(',') && SPACING)
#define EOT !ANY

#define HEX_DIGIT CHAR_SET("0-9a-fA-F")
#define HEX_PREFIX (LITERAL_ICASE("0x"))

int translation_unit();
int external_declaration();
int function_definition();
int declarator();
int declaration_list();
int compound_statement();
int declaration();
int declaration_specifiers();
int init_declarator_list();
int storage_class_specifier();
int type_qualifier();
int function_specifier();
int typedef_name();
int init_declarator();
int initializer();
int struct_or_union_specifier();
int enum_specifier();
int identifier();
int struct_declaration();
int specifier_qualifier_list();
int struct_declarator_list();
int struct_declarator();
int constant_expression();
int enumerator_list();
int enumerator();
int enumeration_constant();
int pointer();
int direct_declarator();
int assignment_expression();
int parameter_type_list();
int identifier_list();
int parameter_list();
int parameter_declaration();
int abstract_declarator();
int direct_abstract_declarator();
int initializer_list();
int designation();
int designator();
int labeled_statement();
int expression_statement();
int selection_statement();
int iteration_statement();
int jump_statement();
int expression();
int constant();
int string_literal();
int argument_expression_list();
int unary_operator();
int cast_expression();
int assignment_operator();
int type_specifier();
int keyword();
int float_constant();
int integer_constant();
int character_constant();
int float_constant();
int decimal_float_constant();
int hex_float_constant();
int fraction();
int hexFraction();
int exponent();
int binary_exponent();
#define FLOAT_SUFFIX CHAR_SET("flFL")
int enumeration_constant();
int character_constant();
int Char();
int escape();
int simple_escape();
int octal_escape();
int hex_escape();
int hex_fraction();
int decimal_constant();
int hex_constant();
int octal_constant();
int integer_suffix();
/*
int ();
int ();
int ();
int ();
int ();
int ();*/

// TranslationUnit <- Spacing ( ExternalDeclaration / SEMI ) * EOT
int translation_unit()
{
  return SPACING && SEQUENCE(ZERO_OR_MORE(external_declaration() || SEMI,,),,) && EOT;
}

// ExternalDeclaration <- FunctionDefinition / Declaration
int external_declaration()
{
  return function_definition() || declaration();
}

// FunctionDefinition <- DeclarationSpecifiers Declarator DeclarationList? CompoundStatement
int function_definition()
{
  return declaration_specifiers() && declarator()
      && OPTIONAL(declaration_list(),,) && compound_statement();
}

// DeclarationList <- Declaration+
int declaration_list()
{
  return ONE_OR_MORE(declaration(),,);
}

// Declaration <- DeclarationSpecifiers InitDeclaratorList? SEMI
int declaration()
{

  return SEQUENCE(declaration_specifiers() && OPTIONAL(init_declarator_list(),,) && SEMI,,);
}

/*
DeclarationSpecifiers
   <- (( StorageClassSpecifier
       / TypeQualifier
       / FunctionSpecifier
       )*
       TypedefName
       ( StorageClassSpecifier
       / TypeQualifier
       / FunctionSpecifier
       )*
      )     #{DeclarationSpecifiers}
    / ( StorageClassSpecifier
      / TypeSpecifier
      / TypeQualifier
      / FunctionSpecifier
      )+    #{DeclarationSpecifiers}
*/
int declaration_specifiers()
{
  return SEQUENCE(
    ZERO_OR_MORE(storage_class_specifier() || type_qualifier() || function_specifier(),,)
    && typedef_name() &&
    ZERO_OR_MORE(storage_class_specifier() || type_qualifier() || function_specifier(),,)
  ,,)
  ||
  ONE_OR_MORE(
    storage_class_specifier() || type_specifier() || type_qualifier()
        || function_specifier()
  ,,);
}

//InitDeclaratorList <- InitDeclarator (COMMA InitDeclarator)*
int init_declarator_list()
{
  return init_declarator() && ZERO_OR_MORE(COMMA && init_declarator(),,);
}

// InitDeclarator <- Declarator (EQU Initializer)? #{}
int init_declarator()
{
  return declarator() && OPTIONAL(EQU && initializer(),,);
}

/*
StorageClassSpecifier
   <- TYPEDEF
    / EXTERN
    / STATIC
    / AUTO
    / REGISTER
    / ATTRIBUTE LPAR LPAR (!RPAR .)* RPAR RPAR
*/
int storage_class_specifier()
{
  return SEQUENCE(( TYPEDEF  || EXTERN || STATIC || AUTO || REGISTER ||
    (ATTRIBUTE && LPAR && LPAR && ZERO_OR_MORE(NEXT_CHR_IS_NOT(')') && ANY,,) && RPAR && RPAR)),,);
}
/*
TypeSpecifier
   <- VOID
    / CHAR
    / SHORT
    / INT
    / LONG
    / FLOAT
    / DOUBLE
    / SIGNED
    / UNSIGNED
    / BOOL
    / COMPLEX
    / StructOrUnionSpecifier
    / EnumSpecifier
*/
int type_specifier()
{
  return SEQUENCE( VOID || CHAR || SHORT || INT || LONG || FLOAT
      || DOUBLE || SIGNED || UNSIGNED || BOOL || COMPLEX
      || struct_or_union_specifier() || enum_specifier(),,);
}
/*
StructOrUnion <- STRUCT / UNION

StructOrUnionSpecifier
   <- StructOrUnion
      ( Identifier? LWING StructDeclaration* RWING
      / Identifier
      )
*/
int struct_or_union_specifier()
{
  return SEQUENCE( ( STRUCT || UNION) &&
      (
        (
          SEQUENCE(OPTIONAL(identifier(),,) && LWING && ZERO_OR_MORE(struct_declaration(),,) && RWING,,)
        ||
          identifier()
        )
      ),,);
}

// StructDeclaration <- ( SpecifierQualifierList StructDeclaratorList? )? SEMI
int struct_declaration()
{
  return SEQUENCE(OPTIONAL(specifier_qualifier_list() && OPTIONAL(struct_declarator_list(),,),,)
      && SEMI,,);
}

/*
SpecifierQualifierList
   <- ( TypeQualifier*
        TypedefName
        TypeQualifier*
      )
    / ( TypeSpecifier
      / TypeQualifier
      )+
*/
int specifier_qualifier_list()
{
  return SEQUENCE((ZERO_OR_MORE(type_qualifier(),,) && typedef_name()
      && ZERO_OR_MORE(type_qualifier(),,)),,)
  ||
  SEQUENCE(ONE_OR_MORE(type_specifier() || type_qualifier(),,),,);
}

// StructDeclaratorList <- StructDeclarator (COMMA StructDeclarator)*
int struct_declarator_list()
{
  return struct_declarator() &&
      ZERO_OR_MORE(COMMA && struct_declarator(),,);
}

/*
StructDeclarator
   <- Declarator? COLON ConstantExpression
    / Declarator
*/
int struct_declarator()
{
  return SEQUENCE(OPTIONAL(declarator(),,) && COLON && constant_expression(),,)
  || declarator();
}

/*
EnumSpecifier
    <- ENUM
      ( Identifier? LWING EnumeratorList COMMA? RWING
      / Identifier
      )
*/
int enum_specifier()
{
  return SIMPLE_SEQUENCE(ENUM &&
          (SEQUENCE(OPTIONAL(identifier(),,) && LWING
      && enumerator_list() && OPTIONAL(COMMA,,) && RWING,,)
      || identifier()));
}

// EnumeratorList <- Enumerator (COMMA Enumerator)*
int enumerator_list()
{
  return enumerator() && ZERO_OR_MORE(COMMA && enumerator(),,);
}

// Enumerator <- EnumerationConstant (EQU ConstantExpression)?
int enumerator()
{
  return enumeration_constant() && OPTIONAL(EQU && constant_expression(),,);
}

/*
TypeQualifier
   <- CONST
    / RESTRICT
    / VOLATILE
    / DECLSPEC LPAR Identifier RPAR
*/
int type_qualifier()
{
  return SIMPLE_SEQUENCE(CONST || RESTRICT || VOLATILE
      || (DECLSPEC && LPAR && identifier() && RPAR));
}

// FunctionSpecifier <- INLINE / STDCALL
int function_specifier()
{
  return SIMPLE_SEQUENCE(INLINE || STDCALL);
}

// Declarator <- Pointer? DirectDeclarator
int declarator()
{
  return OPTIONAL(pointer(),,) && direct_declarator();
}

/*
DirectDeclarator
   <- ( Identifier
      / LPAR Declarator RPAR
      )
      ( LBRK TypeQualifier* AssignmentExpression? RBRK
      / LBRK STATIC TypeQualifier* AssignmentExpression RBRK
      / LBRK TypeQualifier+ STATIC AssignmentExpression RBRK
      / LBRK TypeQualifier* STAR RBRK
      / LPAR ParameterTypeList RPAR
      / LPAR IdentifierList? RPAR
      )* #{}
*/
//( LBRK TypeQualifier* AssignmentExpression? RBRK
int direct_declarator_1()
{
  return SEQUENCE(LBRK && ZERO_OR_MORE(type_qualifier(),,)
      && OPTIONAL(assignment_expression(),,) && RBRK,,);
}

// LBRK STATIC TypeQualifier* AssignmentExpression RBRK
int direct_declarator_2()
{
  return SEQUENCE(LBRK && STATIC && ZERO_OR_MORE(type_qualifier(),,)
      && assignment_expression() && RBRK,,);
}

// LBRK TypeQualifier+ STATIC AssignmentExpression RBRK
int direct_declarator_3()
{
  return SEQUENCE(LBRK && ONE_OR_MORE(type_qualifier(),,)
        && STATIC && assignment_expression() && RBRK,,);
}

// LBRK TypeQualifier* STAR RBRK
int direct_declarator_4()
{
  return SEQUENCE(LBRK && ZERO_OR_MORE(type_qualifier(),,) && STAR && RBRK,,);
}

// LPAR ParameterTypeList RPAR
int direct_declarator_5()
{
  return SEQUENCE(LPAR && parameter_type_list() && RPAR,,);
}

// LPAR IdentifierList? RPAR
int direct_declarator_6()
{
  return SEQUENCE(LPAR && OPTIONAL(identifier_list(),,) && RPAR,,);
}

int direct_declarator()
{
  return SEQUENCE((identifier() ||
    (LPAR && declarator() && RPAR))
  && ZERO_OR_MORE(direct_declarator_1() || direct_declarator_2()
     || direct_declarator_3() || direct_declarator_4()
     || direct_declarator_5() || direct_declarator_6(),,),,);
}

//Pointer <- ( STAR TypeQualifier* )+
int pointer()
{
  return ZERO_OR_MORE(STAR && ZERO_OR_MORE(type_qualifier(),,),,);
}

//ParameterTypeList <- ParameterList (COMMA ELLIPSIS)?
int parameter_type_list()
{
  return parameter_list() &&
      OPTIONAL(COMMA && ELLIPSIS,,);
}

// ParameterList <- ParameterDeclaration (COMMA ParameterDeclaration)*
int parameter_list()
{
  return parameter_declaration()
        && ZERO_OR_MORE(COMMA && parameter_declaration(),,);
}

/*
ParameterDeclaration
   <- DeclarationSpecifiers
      ( Declarator
      / AbstractDeclarator
      )?
*/
int parameter_declaration()
{
  return declaration_specifiers()
      && OPTIONAL(declarator() || abstract_declarator(),,);
}

// IdentifierList <- Identifier (COMMA Identifier)*
int identifier_list()
{
  return identifier()
      && ZERO_OR_MORE(COMMA && identifier(),,);
}

// TypeName <- SpecifierQualifierList AbstractDeclarator?
int type_name()
{
  return specifier_qualifier_list() && OPTIONAL(abstract_declarator(),,);
}

/*
AbstractDeclarator
   <- Pointer? DirectAbstractDeclarator
    / Pointer
*/
int abstract_declarator()
{
  return (OPTIONAL(pointer(),,) && direct_abstract_declarator())
    || pointer();
}

// LPAR AbstractDeclarator RPAR
int direct_abstract_declarator_1()
{
  return SEQUENCE(LPAR && abstract_declarator()&& RPAR,,);
}

// LBRK (AssignmentExpression / STAR)? RBRK
int direct_abstract_declarator_2()
{
  return SEQUENCE(LBRK && OPTIONAL(assignment_expression() || STAR,,) && RBRK,,);
}

// LPAR ParameterTypeList? RPAR
int direct_abstract_declarator_3()
{
  return SEQUENCE(LPAR && OPTIONAL(parameter_type_list(),,) && RPAR,,);
}

/*
  DirectAbstractDeclarator
    <- ( LPAR AbstractDeclarator RPAR
      / LBRK (AssignmentExpression / STAR)? RBRK
      / LPAR ParameterTypeList? RPAR
      )
      ( LBRK (AssignmentExpression / STAR)? RBRK
      / LPAR ParameterTypeList? RPAR
      )*
*/
int direct_abstract_declarator()
{
  return SEQUENCE(direct_abstract_declarator_1() || direct_abstract_declarator_2() ||
    direct_abstract_declarator_3(),,) &&
    SEQUENCE(direct_abstract_declarator_2() || direct_abstract_declarator_3(),,);
}

// TypedefName <-Identifier
int typedef_name()
{
  return identifier();
}

/*
  Initializer
   <- AssignmentExpression
    / LWING InitializerList COMMA? RWING
*/
int initializer()
{
  return assignment_expression() ||
    SEQUENCE(LWING && initializer_list() && OPTIONAL(COMMA,,) && RWING,,);
}

//InitializerList <- Designation? Initializer (COMMA Designation? Initializer)*
int initializer_list()
{
  return SEQUENCE(OPTIONAL(designation(),,) && initializer() &&
      ZERO_OR_MORE(COMMA && OPTIONAL(designation(),,) && initializer(),,),,);
}

// Designation <- Designator+ EQU
int designation()
{
  return SEQUENCE(ONE_OR_MORE(designator(),,) && EQU,,);
}

/*
Designator
   <- LBRK ConstantExpression RBRK
    / DOT Identifier
*/
int designator()
{
  return SEQUENCE(LBRK && constant_expression() && RBRK,,)
    || SEQUENCE(DOT && identifier(),,);
}

/*
Statement
   <- LabeledStatement
    / CompoundStatement
    / ExpressionStatement
    / SelectionStatement
    / IterationStatement
    / JumpStatement
*/
int statement()
{
  return labeled_statement() || compound_statement() || expression_statement() ||
        selection_statement() || iteration_statement() || jump_statement();
}

/*
LabeledStatement
   <- Identifier COLON Statement
    / CASE ConstantExpression COLON Statement
    / DEFAULT COLON Statement
*/
int labeled_statement()
{
  return SEQUENCE(identifier() && COLON && statement(),,)
  ||
  SEQUENCE(CASE && constant_expression() && COLON && statement(),,)
  ||
  SEQUENCE(DEFAULT && COLON && statement(),,);
}

// CompoundStatement <- LWING ( Declaration / Statement )* RWING
int compound_statement()
{
  return SEQUENCE(LWING && ZERO_OR_MORE(declaration() || statement(),,) && RWING,,);
}

// ExpressionStatement <- Expression? SEMI
int expression_statement()
{
  return SEQUENCE(OPTIONAL(expression(),,) && SEMI,,);
}

/*
SelectionStatement
   <- IF LPAR Expression RPAR Statement (ELSE Statement)?
    / SWITCH LPAR Expression RPAR Statement
*/
int selection_statement()
{
  return SEQUENCE(IF && LPAR && expression() && RPAR && statement()
      && OPTIONAL(ELSE && statement(),,),,)
    ||
      SEQUENCE(SWITCH && LPAR && expression() && RPAR && statement(),,);
}

// WHILE LPAR Expression RPAR Statement
int iteration_statement_1()
{
  return SEQUENCE(WHILE && LPAR && expression() && RPAR && statement(),,);
}

// DO Statement WHILE LPAR Expression RPAR SEMI
int iteration_statement_2()
{
  return SEQUENCE(DO && statement() && WHILE && LPAR && expression() && RPAR && SEMI,,);
}

// FOR LPAR Expression? SEMI Expression? SEMI Expression? RPAR Statement
int iteration_statement_3()
{
  return SEQUENCE(FOR && LPAR &&
        OPTIONAL(expression(),,) && SEMI &&
        OPTIONAL(expression(),,) && SEMI &&
        OPTIONAL(expression(),,) && RPAR && statement(),,);
}

// FOR LPAR Declaration Expression? SEMI Expression? RPAR Statement
int iteration_statement_4()
{
  return SEQUENCE(FOR && LPAR && declaration() &&
      OPTIONAL(expression(),,) && SEMI &&
      OPTIONAL(expression(),,) && RPAR && statement(),,);
}

/*
IterationStatement
   <- WHILE LPAR Expression RPAR Statement
    / DO Statement WHILE LPAR Expression RPAR SEMI
    / FOR LPAR Expression? SEMI Expression? SEMI Expression? RPAR Statement
    / FOR LPAR Declaration Expression? SEMI Expression? RPAR Statement
*/

int iteration_statement()
{
  return iteration_statement_1() || iteration_statement_2()
      || iteration_statement_3() || iteration_statement_4();
}

// GOTO Identifier SEMI
int goto_jump_statement()
{
  return SEQUENCE(GOTO && identifier() && SEMI,,);
}

//  CONTINUE SEMI
int continue_jump_statement()
{
  return SEQUENCE(CONTINUE && SEMI,,);
}

//  BREAK SEMI
int break_jump_statement()
{
  return SEQUENCE(BREAK && SEMI,,);
}

// RETURN Expression? SEMI
int return_jump_statement()
{
  return SEQUENCE(RETURN && OPTIONAL(expression(),,) && SEMI,,);
}

/*
JumpStatement
   <- GOTO Identifier SEMI
    / CONTINUE SEMI
    / BREAK SEMI
    / RETURN Expression? SEMI
*/

int jump_statement()
{
  return goto_jump_statement() || continue_jump_statement()
      || break_jump_statement() || return_jump_statement();
}

/*
PrimaryExpression
   <- StringLiteral
    / Constant
    / Identifier
    / LPAR Expression RPAR
*/
int primary_expression()
{
  return string_literal() || constant() || identifier() ||
    SEQUENCE(LPAR && expression() && RPAR,,);
}

/*
PostfixExpression
   <- ( PrimaryExpression
      / LPAR TypeName RPAR LWING InitializerList COMMA? RWING
      )
      ( LBRK Expression RBRK
      / LPAR ArgumentExpressionList? RPAR
      / DOT Identifier
      / PTR Identifier
      / INC
      / DEC
      )*
*/
int postfix_expression_1()
{
  return SEQUENCE(LPAR && type_name() && RPAR
    && LWING && initializer_list() &&
    OPTIONAL(COMMA,,) && RWING,,);
}

int postfix_expression_2()
{
  return SEQUENCE(LBRK && expression() && RBRK,,);
}

int postfix_expression_3()
{
  return SEQUENCE(LPAR && ZERO_OR_MORE(argument_expression_list(),,) && RPAR,,);
}

int postfix_expression_4()
{
  return SEQUENCE(DOT && identifier(),,);
}

int postfix_expression_5()
{
  return SEQUENCE(PTR && identifier(),,);
}

int postfix_expression_6()
{
  return SEQUENCE(INC || DEC,,);
}

int postfix_expression()
{
  return SEQUENCE((primary_expression() || postfix_expression_1())
  && ZERO_OR_MORE(postfix_expression_2() || postfix_expression_3() ||
        postfix_expression_3() || postfix_expression_5()
        || postfix_expression_6(),,),,);
}

int argument_expression_list()
{
  return assignment_expression()
      && ZERO_OR_MORE(COMMA && assignment_expression(),,);
}

/*
UnaryExpression
   <- PostfixExpression
    / INC UnaryExpression
    / DEC UnaryExpression
    / UnaryOperator CastExpression
    / SIZEOF (UnaryExpression / LPAR TypeName RPAR )
*/
int unary_expression()
{
  return postfix_expression()
    ||
    SEQUENCE(INC && unary_expression(),,)
    ||
    SEQUENCE(DEC && unary_expression(),,)
    ||
    SEQUENCE(unary_operator() && cast_expression(),,)
    ||
    SEQUENCE(SIZEOF && (unary_expression() ||
              SEQUENCE(LPAR && type_name() && RPAR,,)),,);
}

/*
UnaryOperator
   <- AND
    / STAR
    / PLUS
    / MINUS
    / TILDA
    / BANG
*/

int unary_operator()
{
  return SEQUENCE(AND || STAR || PLUS || MINUS || TILDA || BANG,,);
}


// CastExpression <- (LPAR TypeName RPAR CastExpression) / UnaryExpression
int cast_expression()
{
  return SEQUENCE(LPAR && type_name()
        && RPAR && cast_expression(),,)
    ||
      unary_expression();
}


// MultiplicativeExpression <- CastExpression ((STAR / DIV / MOD) CastExpression)*
int multiplicative_expression()
{
  return cast_expression() &&
      ZERO_OR_MORE((STAR || DIV || MOD) && cast_expression(),,);
}


//AdditiveExpression <- MultiplicativeExpression ((PLUS / MINUS) MultiplicativeExpression)*
int additive_expression()
{
  return multiplicative_expression() &&
      ZERO_OR_MORE((PLUS || MINUS) && multiplicative_expression(),,);
}

//ShiftExpression <- AdditiveExpression ((LEFT / RIGHT) AdditiveExpression)*
int shift_expression()
{
  return SEQUENCE(additive_expression() && ZERO_OR_MORE(( LEFT || RIGHT) && additive_expression(),,),,);
}

//RelationalExpression <- ShiftExpression ((LE / GE / LT / GT) ShiftExpression)*
int relational_expression()
{
  return SEQUENCE(shift_expression() && ZERO_OR_MORE((LE || GE || LT || GT) && shift_expression(),,),,);
}


//EqualityExpression <- RelationalExpression ((EQUEQU / BANGEQU) RelationalExpression)*
int equality_expression()
{
  return SEQUENCE(relational_expression() &&
        ZERO_OR_MORE((EQUEQU || BANGEQU) && relational_expression(),,),,);
}

//ANDExpression <- EqualityExpression (AND EqualityExpression)*
int and_expression()
{
  return SEQUENCE(equality_expression() && ZERO_OR_MORE(AND && equality_expression(),,),,);
}

//ExclusiveORExpression <- ANDExpression (HAT ANDExpression)*
int exclusive_or_expression()
{
  return SEQUENCE(and_expression() && ZERO_OR_MORE(HAT && and_expression(),,),,);
}

//InclusiveORExpression <- ExclusiveORExpression (OR ExclusiveORExpression)*
int inclusive_or_expression()
{
  return SEQUENCE(exclusive_or_expression() && ZERO_OR_MORE(OR && exclusive_or_expression(),,),,);
}

//LogicalANDExpression <- InclusiveORExpression (ANDAND InclusiveORExpression)*
int logical_and_expression()
{
  return SEQUENCE(inclusive_or_expression() &&
      ZERO_OR_MORE(ANDAND && inclusive_or_expression(),,),,);
}

//LogicalORExpression <- LogicalANDExpression (OROR LogicalANDExpression)*
int logical_or_expression()
{
  return SEQUENCE(logical_and_expression() &&
      ZERO_OR_MORE(OROR && logical_and_expression(),,),,);
}

//ConditionalExpression <- LogicalORExpression (QUERY Expression COLON LogicalORExpression)*
int conditional_expression()
{
  return SEQUENCE(logical_or_expression() &&
      ZERO_OR_MORE(QUERY && expression() && COLON && logical_or_expression(),,),,);
}

/*
AssignmentExpression
   <- UnaryExpression AssignmentOperator AssignmentExpression
    / ConditionalExpression
*/
int assignment_expression()
{
  return SEQUENCE(unary_expression() && assignment_operator() && assignment_expression(),,)
        || conditional_expression();
}

/*
AssignmentOperator
   <- EQU
    / STAREQU
    / DIVEQU
    / MODEQU
    / PLUSEQU
    / MINUSEQU
    / LEFTEQU
    / RIGHTEQU
    / ANDEQU
    / HATEQU
    / OREQU
*/

int assignment_operator()
{
  return SEQUENCE(EQU || STAREQU || DIVEQU || MODEQU || PLUSEQU || MINUSEQU
        || LEFTEQU || RIGHTEQU || ANDEQU || HATEQU || OREQU,,);
}

// Expression <- AssignmentExpression (COMMA AssignmentExpression)*
int expression()
{
  return assignment_expression() && ZERO_OR_MORE(COMMA && assignment_expression(),,);
}

// ConstantExpression <- ConditionalExpression
int constant_expression()
{
  return conditional_expression();
}

/*
//Expression <- AssignmentExpression (COMMA AssignmentExpression)*
int expression()
{
  return assignment_expression() &&
      ZERO_OR_MORE(COMMA) && assignment_expression(),,);
}
*/

//EnumerationConstant <- Identifier
int enumeration_constant()
{
  return SEQUENCE(identifier(),,);
}

// StringLiteral <- 'L'? (["] StringChar* ["] Spacing)+
int string_literal()
{
  return SEQUENCE(OPTIONAL(NEXT_CHR=='L',,) && ONE_OR_MORE(DOUBLE_QUOTED_STRING && SPACING,,),,);
}

// Identifier <- !Keyword IdNondigit IdChar* Spacing #{}
int identifier()
{
  return SEQUENCE(!keyword() && ID_NONDIGIT && ZERO_OR_MORE(ID_CHAR,,) && SPACING,,);
}

/*
Constant
   <- FloatConstant
    / IntegerConstant       # Note: can be a prefix of Float Constant!
    / EnumerationConstant
    / CharacterConstant
*/
int constant()
{
  return float_constant() || integer_constant() || enumeration_constant() || character_constant();
}

#define LSUFFIX (LITERAL_ICASE("ll"))

/*
IntegerConstant
   <- ( DecimalConstant
      / HexConstant
      / OctalConstant
      )
    IntegerSuffix? Spacing
*/
int integer_constant()
{
  return SEQUENCE((decimal_constant() || hex_constant() || octal_constant())
        && OPTIONAL(integer_suffix(),,) && SPACING,,);
}

//DecimalConstant <- [1-9][0-9]*
int decimal_constant()
{
  return SEQUENCE(CHAR_SET("1-9") && ZERO_OR_MORE(CHAR_SET("0-9"),,),,);
}

//OctalConstant   <- '0' [0-7]*
int octal_constant()
{
  return SEQUENCE(NEXT_CHR_IS('0') && ZERO_OR_MORE(CHAR_SET("0-7"),,),,);
}

//HexConstant     <- HexPrefix HexDigit+
int hex_constant()
{
  return SEQUENCE(HEX_PREFIX && ONE_OR_MORE(HEX_DIGIT,,),,);
}

/*
IntegerSuffix
   <- [uU] Lsuffix?
    / Lsuffix [uU]?
*/
int integer_suffix()
{
  return SEQUENCE(CHAR_SET("uU") && OPTIONAL(LSUFFIX,,),,)
    ||
      SEQUENCE(LSUFFIX && OPTIONAL(CHAR_SET("uU"),,),,);
}

/*
FloatConstant
   <- ( DecimalFloatConstant
      / HexFloatConstant
      )
    FloatSuffix? Spacing
*/
int float_constant()
{
  return SEQUENCE((decimal_float_constant() || hex_float_constant())
      && OPTIONAL(FLOAT_SUFFIX,,),,) && SPACING;
}
/*
DecimalFloatConstant
   <- Fraction Exponent?
    / [0-9]+ Exponent
*/
int decimal_float_constant()
{
  return SEQUENCE((fraction() && OPTIONAL(exponent(),,))
    || (ONE_OR_MORE(CHAR_SET("0-9"),,) && exponent()),,);
}

/*HexFloatConstant
   <- HexPrefix HexFraction BinaryExponent?
    / HexPrefix HexDigit+ BinaryExponent
*/
int hex_float_constant()
{
  return SEQUENCE(HEX_PREFIX && hex_fraction() && OPTIONAL(binary_exponent(),,),,)
    || SEQUENCE(HEX_PREFIX && ONE_OR_MORE(HEX_DIGIT,,) && binary_exponent(),,);
}
/*
Fraction
   <- [0-9]* '.' [0-9]+
    / [0-9]+ '.'
*/
int fraction()
{
  return SEQUENCE(ZERO_OR_MORE(CHAR_SET("0-9"),,) &&  NEXT_CHR_IS('.') && ONE_OR_MORE(CHAR_SET("0-9"),,),,)
  || SEQUENCE(ONE_OR_MORE(CHAR_SET("0-9"),,) && NEXT_CHR_IS('.'),,);
}

/*
HexFraction
   <- HexDigit* '.' HexDigit+
    / HexDigit+ '.'
*/
int hex_fraction()
{
  return SEQUENCE(ZERO_OR_MORE(HEX_DIGIT,,) && NEXT_CHR_IS('.') && ONE_OR_MORE(HEX_DIGIT,,),,)
    ||
    SEQUENCE(ONE_OR_MORE(HEX_DIGIT,,) && NEXT_CHR_IS('.'),,);
}

//Exponent <- [eE][+\-]? [0-9]+
int exponent()
{
  return SEQUENCE(CHAR_SET("eE") && OPTIONAL(CHAR_SET("-+"),,) && ONE_OR_MORE(CHAR_SET("0-9"),,),,);
}

//BinaryExponent <- [pP][+\-]? [0-9]+
int binary_exponent()
{
  return SEQUENCE(CHAR_SET("pP") && OPTIONAL(CHAR_SET("-+"),,) && ONE_OR_MORE(CHAR_SET("0-9"),,),,);
}

//EnumerationConstant <- Identifier
int enumerator_constant()
{
  return identifier();
}

//CharacterConstant <- 'L'? ['] Char* ['] Spacing
int character_constant()
{
  return SEQUENCE(OPTIONAL(NEXT_CHR_IS('L'),,) && NEXT_CHR_IS('\'')
      && ZERO_OR_MORE(Char(),,) && NEXT_CHR_IS('\''),,);
}

//Char <- Escape / !['\n\\] .
int Char()
{
  return escape() || SEQUENCE(!CHAR_SET("'\n\\") && ANY,,);
}

/*
Escape
   <- SimpleEscape
    / OctalEscape
    / HexEscape
    / UniversalCharacter
*/
int escape()
{
  return simple_escape() || octal_escape() || hex_escape(); // || universal_character();
}

// SimpleEscape <- '\\' ['\"?\\%abfnrtv]
int simple_escape()
{
  return SEQUENCE(NEXT_CHR_IS('\\') && CHAR_SET("'\"?\\%abfnrtv"),,);
}

//OctalEscape  <- '\\' [0-7][0-7]?[0-7]?
int octal_escape()
{
  return SEQUENCE(NEXT_CHR_IS('\\') && CHAR_SET("0-7") && OPTIONAL(CHAR_SET("0-7"),,) && OPTIONAL(CHAR_SET("0-7"),,),,);
}

//HexEscape    <- '\\x' HexDigit+
int hex_escape()
{
  return SEQUENCE(LITERAL("\\x") && ONE_OR_MORE(HEX_DIGIT,,),,);
}

int keyword() {
  return SIMPLE_SEQUENCE(
    (
      LITERAL("auto") ||
      LITERAL("break") ||
      LITERAL("case") ||
      LITERAL("char") ||
      LITERAL("const") ||
      LITERAL("continue") ||
      LITERAL("default") ||
      LITERAL("double") ||
      LITERAL("do") ||
      LITERAL("else") ||
      LITERAL("enum") ||
      LITERAL("extern") ||
      LITERAL("float") ||
      LITERAL("for") ||
      LITERAL("goto") ||
      LITERAL("if") ||
      LITERAL("int") ||
      LITERAL("inline") ||
      LITERAL("long") ||
      LITERAL("register") ||
      LITERAL("restrict") ||
      LITERAL("return") ||
      LITERAL("short") ||
      LITERAL("signed") ||
      LITERAL("sizeof") ||
      LITERAL("static") ||
      LITERAL("struct") ||
      LITERAL("switch") ||
      LITERAL("typedef") ||
      LITERAL("union") ||
      LITERAL("unsigned") ||
      LITERAL("void") ||
      LITERAL("volatile") ||
      LITERAL("while") ||
      LITERAL("_Bool") ||
      LITERAL("_Complex") ||
      LITERAL("_Imaginary") ||
      LITERAL("_stdcall") ||
      LITERAL("__declspec") ||
      LITERAL("__attribute__")
    ) && !ID_CHAR
  );
}


int main(int argc,char* argv[])
{
  A_SEE_PARSER_INIT;
  if(translation_unit())
    puts("PAss");
  else {
    puts("Fail");
    printf("%.50s\n",__acp_ptr__->input_.buffer_+__acp_ptr__->max_pos_);
  }
  A_SEE_PARSER_DESTROY;
}
